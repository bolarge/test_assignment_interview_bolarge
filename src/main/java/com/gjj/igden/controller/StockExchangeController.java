package com.gjj.igden.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gjj.igden.model.Exchange;
import com.gjj.igden.service.ExchangeService;
import com.gjj.igden.utils.Message;
import com.gjj.igden.utils.UrlUtil;

@RequestMapping("/xch_mngr") 
@Controller
public class StockExchangeController {
	
	private final Logger logger = LoggerFactory.getLogger(StockExchangeController.class);
	@Autowired
	private ExchangeService exchangeService;
	@Autowired
	private MessageSource messageSource;
	
	//List Stock Exchanges
	@RequestMapping(method = RequestMethod.GET)
	public String list(Model uiModel) {
		this.logger.info("List all stock exchanges");
		List<Exchange> stockExchanges = this.exchangeService.findAll();
		uiModel.addAttribute("stockExchanges", stockExchanges);
		return "xch_mngr/list";
	}

	//Display Stock Exchange
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String show(@PathVariable("id") Integer id, Model uiModel) {
		this.logger.info("Displays Stock Exchange");
		Exchange exchange = this.exchangeService.findById(id);
		uiModel.addAttribute("exchange", exchange);
		return "xch_mngr/show";
	}

	//Create Stock Exchange
	@RequestMapping(params = "form", method = RequestMethod.GET)
	public String createForm(Model uiModel) {
		this.logger.info("Renders role form");
		Exchange exchange = new Exchange();
		uiModel.addAttribute("exchange", exchange);
		return "xch_mngr/create";
	}
	
	//Edit an Exchange
	@RequestMapping(value = "/{id}", params = "form", method = RequestMethod.GET)
	public String updateForm(@PathVariable("id") Integer id, Model uiModel) {
		this.logger.info("Render editable exchange form");
		uiModel.addAttribute("exchange", exchangeService.findById(id));
		return "xch_mngr/update";
	}

	//Save an Exchange
	@RequestMapping(params = "form", method = RequestMethod.POST)
	public String create(@Valid Exchange exchange, BindingResult bindingResult, Model uiModel,
			HttpServletRequest httpServletRequest, RedirectAttributes redirectAttributes, Locale locale) {
		logger.info("Saving a stock exchange");
		if (bindingResult.hasErrors()) {
			uiModel.addAttribute("message",new Message("error", messageSource.getMessage("exchange_save_fail", new Object[] {}, locale)));
			uiModel.addAttribute("exchange", exchange);
			return "xch_mngr/create";
		}
		uiModel.asMap().clear();
		redirectAttributes.addFlashAttribute("message",
				new Message("success", messageSource.getMessage("exchange_save_success", new Object[] {}, locale)));
		    
		// Persist an exchange
		exchangeService.saveExchange(exchange);
		logger.info("Exchange id: " + exchange.getId());   
		return "redirect:/xch_mngr/" + UrlUtil.encodeUrlPathSegment(exchange.getId().toString(), httpServletRequest);
	}
	
	//Update an exchange
	@RequestMapping(value = "/{id}", params = "form", method = RequestMethod.POST) 
	public String update(@Valid Exchange exchange, BindingResult bindingResult, Model uiModel,
				HttpServletRequest httpServletRequest, RedirectAttributes redirectAttributes, Locale locale) {
			logger.info("Updating an exchange");
			if (bindingResult.hasErrors()) {
				uiModel.addAttribute("message", new Message("error", messageSource.getMessage("exchange_save_fail", new Object[] {}, locale)));
				uiModel.addAttribute("exchange", exchange);
				return "xch_mngr/update";
			}
			uiModel.asMap().clear();
			redirectAttributes.addFlashAttribute("message", new Message("success",
			messageSource.getMessage("exchange_save_success", new Object[] {}, locale)));		
			
			exchangeService.saveExchange(exchange);
			return "redirect:/rl_mngr/"+ UrlUtil.encodeUrlPathSegment(exchange.getId().toString(), httpServletRequest);
		}
	
}
