/*package com.gjj.igden.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.RememberMeAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.gjj.igden.service.UserProfileService;
import com.gjj.igden.service.UsersService;
import com.gjj.igden.utils.Message;

*//**
 * @author bolarge
 *
 *//*
@Controller

@RequestMapping("/")
@SessionAttributes("roles")
public class SecurityController2 {

	final Logger logger = LoggerFactory.getLogger(SecurityController2.class);	
	
	private MessageSource messageSource;	
    private UsersService userService;     
    private UserProfileService userProfileService;		
	private PersistentTokenBasedRememberMeServices persistentTokenBasedRememberMeServices;	     	
	private AuthenticationTrustResolver authenticationTrustResolver;
	
	@Autowired
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	@Autowired
	public void setUserService(UsersService userService) {
		this.userService = userService;
	}

	@Autowired
	public void setUserProfileService(UserProfileService userProfileService) {
		this.userProfileService = userProfileService;
	}

	@Autowired
	public void setPersistentTokenBasedRememberMeServices(
			PersistentTokenBasedRememberMeServices persistentTokenBasedRememberMeServices) {
		this.persistentTokenBasedRememberMeServices = persistentTokenBasedRememberMeServices;
	}

	@Autowired
	public void setAuthenticationTrustResolver(AuthenticationTrustResolver authenticationTrustResolver) {
		this.authenticationTrustResolver = authenticationTrustResolver;
	}

	@RequestMapping(value = {"/","/welcome"}, method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome Fintech Forex! the client locale is "+ locale.toString());
		return "welcome";
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/loginfail")
	public String loginFail(Model uiModel, Locale locale) {
		logger.info("Login failed detected");
		uiModel.addAttribute("message", new Message("error", messageSource.getMessage("message_login_fail", new Object[]{}, locale))); 
		return "login";
	}

	@RequestMapping(method = RequestMethod.GET, value="/login")
	public String login(Model uiModel, Locale locale) {
		logger.info("Logging in to Fintech Forex "+ locale.toString());	
		if (isCurrentAuthenticationAnonymous()) {
            return "login";
        } else {
            return "redirect:/welcome";  
        }
		//return "welcome"; 
	}
	
	@RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String adminPage(ModelMap model) {
        model.addAttribute("user", getPrincipal());
        return "admin";
    }
 
    @RequestMapping(value = "/db", method = RequestMethod.GET)
    public String dbaPage(ModelMap model) {
        model.addAttribute("user", getPrincipal());
        return "dba";
    }
 
    @RequestMapping(value = "/access_denied", method = RequestMethod.GET)
    public String accessDeniedPage(ModelMap model) {
        model.addAttribute("user", getPrincipal());
        return "access_denied";
    }
    
    *//**
     * This method handles logout requests.
     * Toggle the handlers if you are RememberMe functionality is useless in your app.
     *//*
    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){    
            //new SecurityContextLogoutHandler().logout(request, response, auth);
            this.persistentTokenBasedRememberMeServices.logout(request, response, auth);
            SecurityContextHolder.getContext().setAuthentication(null);
        }
        return "redirect:/login?logout";
    }
    
    *//**
     * This method returns the principal[user-name] of logged-in user.
     *//*
    private String getPrincipal(){
        String userName = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
 
        if (principal instanceof UserDetails) {
            userName = ((UserDetails)principal).getUsername();
        } else {
            userName = principal.toString();
        }
        return userName;
    }
     
    *//**
     * This method returns true if users is already authenticated [logged-in], else false.
     *//*
    private boolean isCurrentAuthenticationAnonymous() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return this.authenticationTrustResolver.isAnonymous(authentication);
    }
    
    *//**
	 * Check if user is login by remember me cookie, refer
	 * org.springframework.security.authentication.AuthenticationTrustResolverImpl
	 *//*
	private boolean isRememberMeAuthenticated() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication == null) {
			return false;
		}
		return RememberMeAuthenticationToken.class.isAssignableFrom(authentication.getClass());
	}

	*//**
	 * save targetURL in session
	 *//*
	private void setRememberMeTargetUrlToSession(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if (session != null) {
			session.setAttribute("targetUrl", "/admin/update");
		}
	}

	*//**
	 * get targetURL from session
	 *//*
	private String getRememberMeTargetUrlFromSession(HttpServletRequest request) {
		String targetUrl = "";
		HttpSession session = request.getSession(false);
		if (session != null) {
			targetUrl = session.getAttribute("targetUrl") == null ? "" : session.getAttribute("targetUrl").toString();
		}
		return targetUrl;
	}
 
}
*/