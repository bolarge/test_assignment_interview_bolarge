package com.gjj.igden.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gjj.igden.model.UserProfile;
import com.gjj.igden.model.Users;
import com.gjj.igden.service.UserProfileService;
import com.gjj.igden.service.UsersService;
import com.gjj.igden.utils.Message;
import com.gjj.igden.utils.UrlUtil;

@RequestMapping("/usr_mngr") 
@Controller
public class UserController {

	private final Logger logger = LoggerFactory.getLogger(UserController.class);
	private MessageSource messageSource;
	private UsersService userService;	
	private UserProfileService userProfileService;
	
	@Autowired
	public void setUserProfileService(UserProfileService userProfileService) {
		this.userProfileService = userProfileService;
	}

	@Autowired
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	@Autowired
	public void setUserService(UsersService userService) {
		this.userService = userService;
	}

	//List Accounts
	@RequestMapping(method = RequestMethod.GET)
	public String list(Model uiModel) {
		this.logger.info("Retrieving all users");
		List<Users> users = this.userService.findAllUsers();
		logger.info("Inside Users Controller size is : "+ users.size());
		uiModel.addAttribute("users", users);
		return "usr_mngr/list";
	}

	//Display an Account
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String show(@PathVariable("id") Long id, Model uiModel) {
		this.logger.info("Displays an user");
		Users user = this.userService.findById(id);
		uiModel.addAttribute("user", user);
		return "usr_mngr/show";
	}

	//Create an Account
	@RequestMapping(params = "form", method = RequestMethod.GET) 
	public String createForm(Model uiModel) {
		this.logger.info("Renders new opening account form");
		Users user = new Users();
		uiModel.addAttribute("user", user);
		uiModel.addAttribute("edit", false);
		return "usr_mngr/create";
	}
	
	//Edit a Account
	@RequestMapping(value = "/{id}", params = "form", method = RequestMethod.GET)
	public String updateForm(@PathVariable("id") Long id, Model uiModel) {
		this.logger.info("Render editable user form");
		uiModel.addAttribute("user", userService.findById(id));
		uiModel.addAttribute("edit", true);
		return "usr_mngr/update";
	}

	// Save an AUser
	@RequestMapping(params = "form", method = RequestMethod.POST)
	public String create(@Valid Users user, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest,
			RedirectAttributes redirectAttributes, Locale locale) {
		logger.info("Before saving account information");
		if (bindingResult.hasErrors()) {
			System.out.println(bindingResult.toString());
			uiModel.addAttribute("message",
					new Message("error", messageSource.getMessage("user_save_fail", new Object[] {}, locale)));
			uiModel.addAttribute("user", user);
			return "usr_mngr/create";
		}
		uiModel.asMap().clear();
		redirectAttributes.addFlashAttribute("message",
				new Message("success", messageSource.getMessage("user_save_success", new Object[] {}, locale)));

		// Encrypt Password
		/*ShaPasswordEncoder encPassword = new ShaPasswordEncoder();
		user.setPassword(encPassword.encodePassword(user.getPassword(), null));*/

		// Persist new Account
		userService.saveUsers(user);
		logger.info("user id: " + user.getId());

		return "redirect:/usr_mngr/" + UrlUtil.encodeUrlPathSegment(user.getId().toString(), httpServletRequest);
	}
	
	// Update an Account
	@RequestMapping(value = "/{id}", params = "form", method = RequestMethod.POST)
	public String update(@Valid Users user, BindingResult bindingResult, Model uiModel,
			HttpServletRequest httpServletRequest, RedirectAttributes redirectAttributes, Locale locale) {
		logger.info("Updating an user");
		if (bindingResult.hasErrors()) {
			uiModel.addAttribute("message",
					new Message("error", messageSource.getMessage("user_save_fail", new Object[] {}, locale)));
			uiModel.addAttribute("user", user);
			return "usr_mngr/update";
		}
		uiModel.asMap().clear();
		redirectAttributes.addFlashAttribute("message",
				new Message("success", messageSource.getMessage("user_save_success", new Object[] {}, locale)));

		/*// Update Password
		ShaPasswordEncoder encPassword = new ShaPasswordEncoder();
		user.setPassword(encPassword.encodePassword(user.getPassword(), null));*/

		userService.updateUsers(user);
		return "redirect:/usr_mngr/" + UrlUtil.encodeUrlPathSegment(user.getId().toString(), httpServletRequest);
	}
	
	@ModelAttribute("roles")
	public List<UserProfile> initializeProfiles() {
		return userProfileService.findAll();
	} 
}