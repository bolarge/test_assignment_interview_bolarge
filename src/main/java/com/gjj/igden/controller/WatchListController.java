package com.gjj.igden.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


import com.gjj.igden.model.WatchListDesc;
import com.gjj.igden.service.WatchListDescService;
import com.gjj.igden.utils.Message;
import com.gjj.igden.utils.UrlUtil;

@RequestMapping("/wsl_mngr") 
@Controller
public class WatchListController {
	
	private final Logger logger = LoggerFactory.getLogger(WatchListController.class);
	private WatchListDescService watchListService;
	private MessageSource messageSource;
	
	@Autowired
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@Autowired
	public void setWatchListService(WatchListDescService watchListService) {
		this.watchListService = watchListService;
	}
	
	// List of all watchlists
	@RequestMapping(method = RequestMethod.GET)
	public String list(Model uiModel) {
		this.logger.info("Retrieving all watchlist");
		List<WatchListDesc> watchLists = this.watchListService.findAll();
		logger.info("Inside WatchList Controller size is : " + watchLists.size());
		uiModel.addAttribute("watchLists", watchLists);
		return "wsl_mngr/list";
	}

	// Display a WatchList
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String show(@PathVariable("id") Integer id, Model uiModel) {
		this.logger.info("Displays a dataset");
		WatchListDesc watchList = this.watchListService.findById(id);
		uiModel.addAttribute("watchList", watchList);
		return "wsl_mngr/show";
	}

	// Create a watchList
	@RequestMapping(params = "form", method = RequestMethod.GET)
	public String createForm(Model uiModel) {
		this.logger.info("Renders a new watchList form");
		WatchListDesc watchList = new WatchListDesc();
		uiModel.addAttribute("watchList", watchList);
		return "wsl_mngr/create";
	}

	// Edit a watchList
	@RequestMapping(value = "/{id}", params = "form", method = RequestMethod.GET)
	public String updateForm(@PathVariable("id") Integer id, Model uiModel) {
		this.logger.info("Render editable watchList form");
		uiModel.addAttribute("watchList", watchListService.findById(id));
		return "wsl_mngr/update";
	}

	// Save an watchList
	@RequestMapping(params = "form", method = RequestMethod.POST)
	public String create(@Valid WatchListDesc watchList, BindingResult bindingResult, Model uiModel,
			HttpServletRequest httpServletRequest, RedirectAttributes redirectAttributes, Locale locale) {
		logger.info("Before saving watchList");
		if (bindingResult.hasErrors()) {
			System.out.println(bindingResult.toString());
			uiModel.addAttribute("message",
			new Message("error", messageSource.getMessage("watchList_save_fail", new Object[] {}, locale)));
			uiModel.addAttribute("watchList", watchList);
			return "wsl_mngr/create";
		}
		uiModel.asMap().clear();
		redirectAttributes.addFlashAttribute("message",
		new Message("success", messageSource.getMessage("watchList_save_success", new Object[] {}, locale)));

		// Persist new WatchList
		watchListService.save(watchList);
		logger.info("watchList id: " + watchList.getWatchListId());
		return "redirect:/wsl_mngr/" + UrlUtil.encodeUrlPathSegment(watchList.getWatchListId().toString(), httpServletRequest);
	}

	// Update WatchList
	@RequestMapping(value = "/{id}", params = "form", method = RequestMethod.POST)
	public String update(@Valid WatchListDesc watchList, BindingResult bindingResult, Model uiModel,
			HttpServletRequest httpServletRequest, RedirectAttributes redirectAttributes, Locale locale) {
		logger.info("Updating a watchList");
		if (bindingResult.hasErrors()) {
			uiModel.addAttribute("message",
			new Message("error", messageSource.getMessage("watchList_save_fail", new Object[] {}, locale)));
			uiModel.addAttribute("watchList", watchList);
			return "wsl_mngr/update";
		}
		uiModel.asMap().clear();
		redirectAttributes.addFlashAttribute("message",
		new Message("success", messageSource.getMessage("watchList_save_success", new Object[] {}, locale)));

		watchListService.save(watchList);
		return "redirect:/wsl_mngr/" + UrlUtil.encodeUrlPathSegment(watchList.getWatchListId().toString(), httpServletRequest);
	}

	/*@RequestMapping(value = "/view-watchlist", method = RequestMethod.GET)
	public String viewAccount(ModelMap model, @RequestParam int id) {
		model.addAttribute("stockSymbolsList", service.getStockSymbolsList(id));
		model.addAttribute("watchListId", id);
		return "view-watchlist";
	}

	@GetMapping(value = "/add-watchlist")
	public String addWatchList(ModelMap model, @RequestParam int id) {
		System.out.println(id);
		WatchListDesc theWatchListDesc = new WatchListDesc(id);
		model.addAttribute("theWatchListDesc", theWatchListDesc);
		return "lazyRowLoad";
	}

	@PostMapping(value = "/lazyRowAdd.web")
	public String lazyRowAdd(@ModelAttribute("theWatchListDesc") WatchListDesc theWatchListDesc,
			@ModelAttribute("username1") String watchlistName, @RequestParam("id") int accId) {
		System.out.println(accId);
		theWatchListDesc.setAccountId(accId);
		theWatchListDesc.setWatchListName(watchlistName);
		service.create(theWatchListDesc);
		return "redirect:/view-account?id=2";
	}*/
}
