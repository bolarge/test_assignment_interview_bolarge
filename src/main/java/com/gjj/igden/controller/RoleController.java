package com.gjj.igden.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.gjj.igden.model.UserProfile;
import com.gjj.igden.referencetypes.UserProfileType;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import javax.validation.Valid;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gjj.igden.service.UserProfileService;
import com.gjj.igden.utils.Message;
import com.gjj.igden.utils.UrlUtil;

@RequestMapping("/rl_mngr")
@Controller
public class RoleController {

	private final Logger logger = LoggerFactory.getLogger(RoleController.class);
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private UserProfileService roleService;
	
	//List ROles
	@RequestMapping(method = RequestMethod.GET)
	public String list(Model uiModel) {
		this.logger.info("Retrieving all roles");
		List<UserProfile> roles = this.roleService.findAll();
		uiModel.addAttribute("roles", roles);
		return "rl_mngr/list";
	}

	//Display an Role
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String show(@PathVariable("id") Integer id, Model uiModel) {
		this.logger.info("Displays an role");
		UserProfile role = this.roleService.findById(id);
		uiModel.addAttribute("role", role);
		return "rl_mngr/show";
	}

	//Create an role
	@RequestMapping(params = "form", method = RequestMethod.GET)
	public String createForm(Model uiModel) {
		this.logger.info("Renders role form");
		UserProfile role = new UserProfile();
		uiModel.addAttribute("role", role);
		return "rl_mngr/create";
	}
	
	//Edit a role
	@RequestMapping(value = "/{id}", params = "form", method = RequestMethod.GET)
	public String updateForm(@PathVariable("id") Integer id, Model uiModel) {
		this.logger.info("Render editable role form");
		uiModel.addAttribute("role", roleService.findById(id));
		return "rl_mngr/update";
	}

	//Save an role @Valid
	@RequestMapping(params = "form", method = RequestMethod.POST)//, 
	public String create(@Valid UserProfile role, BindingResult bindingResult, Model uiModel, @RequestParam(value="file", required=false) Part file,
			HttpServletRequest httpServletRequest, RedirectAttributes redirectAttributes, Locale locale) {
		logger.info("Before saving role information");
		if (bindingResult.hasErrors()) {
			uiModel.addAttribute("message",
					new Message("error", messageSource.getMessage("role_save_fail", new Object[] {}, locale)));
			uiModel.addAttribute("role", role);
			return "rl_mngr/create";
		}
		uiModel.asMap().clear();
		redirectAttributes.addFlashAttribute("message",
				new Message("success", messageSource.getMessage("role_save_success", new Object[] {}, locale)));
		    
		    //Persist new role
		    roleService.save(role);
		    logger.info("user id: " + role.getId());
	   
		return "redirect:/rl_mngr/" + UrlUtil.encodeUrlPathSegment(role.getId().toString(), httpServletRequest);
	}
	
	//Update an role
	@RequestMapping(value = "/{id}", params = "form", method = RequestMethod.POST) 
	public String update(@Valid UserProfile role, BindingResult bindingResult, Model uiModel,
				HttpServletRequest httpServletRequest, RedirectAttributes redirectAttributes, Locale locale, @RequestParam(value="file", required=false) Part file) {
			logger.info("Updating an role");
			if (bindingResult.hasErrors()) {
				uiModel.addAttribute("message", new Message("error", messageSource.getMessage("role_save_fail", new Object[] {}, locale)));
				uiModel.addAttribute("role", role);
				return "rl_mngr/update";
			}
			uiModel.asMap().clear();
			redirectAttributes.addFlashAttribute("message", new Message("success",
					messageSource.getMessage("role_save_success", new Object[] {}, locale)));		
			
			roleService.save(role);
			return "redirect:/rl_mngr/"
					+ UrlUtil.encodeUrlPathSegment(role.getId().toString(), httpServletRequest);
		}
	
}