package com.gjj.igden.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gjj.igden.model.Account;
import com.gjj.igden.model.UserProfile;
import com.gjj.igden.model.WatchListDesc;
import com.gjj.igden.service.AccountService;
import com.gjj.igden.service.UserProfileService;
import com.gjj.igden.service.WatchListDescService;
import com.gjj.igden.utils.Message;
import com.gjj.igden.utils.UploadableService;
import com.gjj.igden.utils.UrlUtil;

@RequestMapping("/acc_mngr") 
@Controller
public class AccountController {

	private final Logger logger = LoggerFactory.getLogger(AccountController.class);
	private MessageSource messageSource;
	private AccountService accountService;	
	private UserProfileService userProfileService;
	private WatchListDescService watchListService;
	private UploadableService uploadService;
	
	@Autowired
	public void setUserProfileService(UserProfileService userProfileService) {
		this.userProfileService = userProfileService;
	}

	@Autowired
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@Autowired
	public void setAccountService(AccountService accountService) {
		this.accountService = accountService;
	}
	
	@Autowired
	public void setWatchListDescService(WatchListDescService watchListDescService) {
		this.watchListService = watchListDescService;
	}
	
	@Autowired
	public void setUploadService(UploadableService uploadService) {
		this.uploadService = uploadService;
	}

	//List Accounts
	@RequestMapping(method = RequestMethod.GET)
	public String list(Model uiModel) {
		this.logger.info("Retrieving all accounts");
		List<Account> accounts = this.accountService.findAll();
		logger.info("Inside Account Controller size is : "+ accounts.size());
		uiModel.addAttribute("accounts", accounts);
		uiModel.addAttribute("principal", getPrincipal());
		return "acc_mngr/list";
	}

	//Display an Account
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String show(@PathVariable("id") Long id, Model uiModel) {
		this.logger.info("Displays an account");
		Account account = this.accountService.findById(id);
		uiModel.addAttribute("account", account);
		return "acc_mngr/show";
	}

	//Create an Account
	@RequestMapping(params = "form", method = RequestMethod.GET) 
	public String createForm(Model uiModel) {
		this.logger.info("Renders new opening account form");
		Account account = new Account();
		uiModel.addAttribute("account", account);
		uiModel.addAttribute("edit", false);
		return "acc_mngr/create";
	}
	
	//Edit a Account
	@RequestMapping(value = "/{id}", params = "form", method = RequestMethod.GET)
	public String updateForm(@PathVariable("id") Long id, Model uiModel) {
		this.logger.info("Render editable account form");
		uiModel.addAttribute("account", accountService.findById(id));
		uiModel.addAttribute("edit", true);
		return "acc_mngr/update";
	}

	//Save an Account
	@RequestMapping(params = "form", method = RequestMethod.POST)
	public String create(@Valid Account account, BindingResult bindingResult, Model uiModel, @RequestParam(value="file", required=false) Part file,
			HttpServletRequest httpServletRequest, RedirectAttributes redirectAttributes, Locale locale) {
		logger.info("Before saving account information");
		if (bindingResult.hasErrors()) {
			System.out.println(bindingResult.toString());
			uiModel.addAttribute("message", new Message("error", messageSource.getMessage("user_save_fail", new Object[] {}, locale)));
			uiModel.addAttribute("account", account);
			return "acc_mngr/create";
		}
		uiModel.asMap().clear();
		redirectAttributes.addFlashAttribute("message",new Message("success", messageSource.getMessage("user_save_success", new Object[] {}, locale)));		
		//Add Image
		account.setImage(uploadService.uploadContent(file));

			//Encrypt Password
			ShaPasswordEncoder encPassword = new ShaPasswordEncoder();
			account.setPassword(encPassword.encodePassword(account.getPassword(), null));
		
		    //Persist new Account
		    accountService.saveAccount(account);
		    logger.info("user id: " + account.getId());
	   
		return "redirect:/acc_mngr/" + UrlUtil.encodeUrlPathSegment(account.getId().toString(), httpServletRequest);
	}
	
	//Update an Account
	@RequestMapping(value = "/{id}", params = "form", method = RequestMethod.POST) 
	public String update(@Valid Account account, BindingResult bindingResult, Model uiModel,
				HttpServletRequest httpServletRequest, RedirectAttributes redirectAttributes, Locale locale, @RequestParam(value="file", required=false) Part file) {
			logger.info("Updating an account");
			if (bindingResult.hasErrors()) {
				uiModel.addAttribute("message", new Message("error", messageSource.getMessage("user_save_fail", new Object[] {}, locale)));
				uiModel.addAttribute("account", account);
				return "acc_mngr/update";
			}
			uiModel.asMap().clear();
			redirectAttributes.addFlashAttribute("message", new Message("success",
					messageSource.getMessage("user_save_success", new Object[] {}, locale)));
			
			//Update Image
			account.setImage(uploadService.uploadContent(file));
			
			//Update Password
			ShaPasswordEncoder encPassword = new ShaPasswordEncoder();
			account.setPassword(encPassword.encodePassword(account.getPassword(), null));
			
			accountService.updateAccount(account);
			return "redirect:/acc_mngr/"
					+ UrlUtil.encodeUrlPathSegment(account.getId().toString(), httpServletRequest);
		}
	
	@ModelAttribute("roles")
	public List<UserProfile> initializeProfiles() {
		return userProfileService.findAll();
	}
	
	@ModelAttribute("stockSymbols")
	public List<WatchListDesc> initializeWatchLists(){
		return watchListService.findAll();
	}
	
	/**
     * This method returns the principal[user-name] of logged-in user.
     */
    private String getPrincipal(){
        String userName = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
 
        if (principal instanceof UserDetails) {
            userName = ((UserDetails)principal).getUsername();
        } else {
            userName = principal.toString();
        }
        return userName;
    }
	
	//Grid Implementation Yet Ready
	/*@RequestMapping(value="/listgrid", method=RequestMethod.GET, produces="application/json")
	  @ResponseBody
	  public AccountGrid listGrid(@RequestParam(value="page", required=false) Integer page, 
			  @RequestParam(value="rows", required=false) Integer rows, 
			  @RequestParam(value="sidx", required=false) String sortBy, 
			  @RequestParam(value="sord", required=false) String order){
		  
	    this.logger.info("Listing persons for grid with page: {}, rows: {}", page, rows);
	    this.logger.info("Listing persons for grid with sort: {}, order: {}", sortBy, order);

	    Sort sort = null;
	    String orderBy = sortBy;
	    if ((orderBy != null) && (orderBy.equals("birthDateString")))
	      orderBy = "birthDate";
	    if ((orderBy != null) && (order != null)) {
	      if (order.equals("desc"))
	        sort = new Sort(Sort.Direction.DESC, new String[] { orderBy });
	      else {
	        sort = new Sort(Sort.Direction.ASC, new String[] { orderBy });
	      }
	    }

	    PageRequest pageRequest = null;
	    if (sort != null)
	      pageRequest = new PageRequest(page.intValue() - 1, rows.intValue(), sort);
	    else {
	      pageRequest = new PageRequest(page.intValue() - 1, rows.intValue());
	    }
	    Page<Account> accountPage = this.accountService.findAllByPage(pageRequest);

	    AccountGrid accountGrid = new AccountGrid();
	    accountGrid.setCurrentPage(accountPage.getNumber() + 1);
	    accountGrid.setTotalPages(accountPage.getTotalPages());
	    accountGrid.setTotalRecords(accountPage.getTotalElements());
	    accountGrid.setAccountData(Lists.newArrayList(accountPage.iterator()));
	    return accountGrid;
	  }*/
	 
}