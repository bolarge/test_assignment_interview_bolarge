package com.gjj.igden.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gjj.igden.model.Instrument;
import com.gjj.igden.service.InstrumentService;
import com.gjj.igden.utils.Message;
import com.gjj.igden.utils.UrlUtil;

public class InstrumentController {
	
	private final Logger logger = LoggerFactory.getLogger(InstrumentController.class);

	@Autowired
	private MessageSource messageSource;
	@Autowired
	private InstrumentService instrumentService;
	
	//List Instrument
	@RequestMapping(method = RequestMethod.GET)
	public String list(Model uiModel) {
		this.logger.info("Retrieving all instrument");
		List<Instrument> instruments = this.instrumentService.findAll();
		uiModel.addAttribute("instruments", instruments);
		return "ins_mngr/list";
	}

	//Display a Instrument
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String show(@PathVariable("id") String id, Model uiModel) {
		this.logger.info("Displays an instrument");
		Instrument instrument = this.instrumentService.findBySymbol(id);
		uiModel.addAttribute("instrument", instrument);
		return "ins_mngr/show";
	}

	// Create a Instrument
	@RequestMapping(params = "form", method = RequestMethod.GET)
	public String createForm(Model uiModel) {
		this.logger.info("Renders instrument form");
		Instrument instrument = new Instrument();
		uiModel.addAttribute("instrument", instrument);
		return "ins_mngr/create";
	}
	
	//Edit a instrument
	@RequestMapping(value = "/{id}", params = "form", method = RequestMethod.GET)
	public String updateForm(@PathVariable("id") String id, Model uiModel) {
		this.logger.info("Render editable instrument form");
		uiModel.addAttribute("instrument", instrumentService.findBySymbol(id));
		return "ins_mngr/update";
	}

	//Save an instrument @Valid
	@RequestMapping(params = "form", method = RequestMethod.POST)//, 
	public String create(@Valid Instrument instrument, BindingResult bindingResult, Model uiModel, @RequestParam(value="file", required=false) Part file,
			HttpServletRequest httpServletRequest, RedirectAttributes redirectAttributes, Locale locale) {
		logger.info("Before saving instrument information");
		if (bindingResult.hasErrors()) {
			uiModel.addAttribute("message",
			new Message("error", messageSource.getMessage("instrument_save_fail", new Object[] {}, locale)));
			uiModel.addAttribute("instrument", instrument);
			return "ins_mngr/create";
		}
		uiModel.asMap().clear();
		redirectAttributes.addFlashAttribute("message",
		new Message("success", messageSource.getMessage("instrument_save_success", new Object[] {}, locale)));
		    
		// Persist new instrument
		instrumentService.saveInstrument(instrument);
		logger.info("user id: " + instrument.getSymbol());   
		return "redirect:/ins_mngr/" + UrlUtil.encodeUrlPathSegment(instrument.getSymbol(), httpServletRequest);
	}
	
	//Update an instrument
	@RequestMapping(value = "/{id}", params = "form", method = RequestMethod.POST) 
	public String update(@Valid Instrument instrument, BindingResult bindingResult, Model uiModel,
				HttpServletRequest httpServletRequest, RedirectAttributes redirectAttributes, Locale locale, @RequestParam(value="file", required=false) Part file) {
			logger.info("Updating an instrument");
			if (bindingResult.hasErrors()) {
				uiModel.addAttribute("message", new Message("error", messageSource.getMessage("instrument_save_fail", new Object[] {}, locale)));
				uiModel.addAttribute("instrument", instrument);
				return "ins_mngr/update";
			}
			uiModel.asMap().clear();
			redirectAttributes.addFlashAttribute("message", new Message("success",
			messageSource.getMessage("instrument_save_success", new Object[] {}, locale)));		
			
			instrumentService.saveInstrument(instrument);
			return "redirect:/ins_mngr/"+ UrlUtil.encodeUrlPathSegment(instrument.getSymbol(), httpServletRequest);
		}
	

}
