package com.gjj.igden.controller;

import com.gjj.igden.model.Bar;
import com.gjj.igden.model.Instrument;
import com.gjj.igden.model.WatchListDesc;
import com.gjj.igden.service.BarService;
import com.gjj.igden.service.InstrumentService;
import com.gjj.igden.service.WatchListDescService;
import com.gjj.igden.utils.Message;
import com.gjj.igden.utils.UrlUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RequestMapping("/mkd_mngr") 
@Controller
public class MarketDataController {
	
	private final Logger logger = LoggerFactory.getLogger(MarketDataController.class);	
	private BarService barService;
	private MessageSource messageSource;
	private WatchListDescService watchListService;
	private InstrumentService instrumentService;
	
	@Autowired
	public void setBarService(BarService barService) {
		this.barService = barService;
	}

	@Autowired
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	@Autowired
	public void setWatchListDescService(WatchListDescService watchListDescService) {
		this.watchListService = watchListDescService;
	}
	
	@Autowired
	public void setInstrumentService(InstrumentService instrumentService) {
		this.instrumentService = instrumentService;
	}

	// List market data
	@RequestMapping(method = RequestMethod.GET)
	public String list(Model uiModel) {
		this.logger.info("Retrieving all market data");
		List<Bar> marketData = this.barService.findAll();
		logger.info("Inside Bar Controller size is : " + marketData.size());
		uiModel.addAttribute("marketData", marketData);
		return "mkd_mngr/list";
	}

	// Display a market data
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String show(@PathVariable("id") Long id, Model uiModel) {
		this.logger.info("Displays a dataset");
		Bar dataset = this.barService.findById(id);
		uiModel.addAttribute("dataset", dataset);
		return "mkd_mngr/show";
	}

	// Create a market data
	@RequestMapping(params = "form", method = RequestMethod.GET)
	public String createForm(Model uiModel) {
		this.logger.info("Renders a new bar form");
		Bar bar = new Bar();
		uiModel.addAttribute("bar", bar);
		return "mkd_mngr/create";
	}

	// Edit a market data
	@RequestMapping(value = "/{id}", params = "form", method = RequestMethod.GET)
	public String updateForm(@PathVariable("id") Long id, Model uiModel) {
		this.logger.info("Render editable bar form");
		uiModel.addAttribute("bar", barService.findById(id));
		return "mkd_mngr/update";
	}

	// Save an market data
	@RequestMapping(params = "form", method = RequestMethod.POST)
	public String create(@Valid Bar marketData, BindingResult bindingResult, Model uiModel,
			HttpServletRequest httpServletRequest, RedirectAttributes redirectAttributes, Locale locale) {
		logger.info("Before saving market data");
		if (bindingResult.hasErrors()) {
			System.out.println(bindingResult.toString());
			uiModel.addAttribute("message",
					new Message("error", messageSource.getMessage("bar_save_fail", new Object[] {}, locale)));
			uiModel.addAttribute("marketData", marketData);
			return "mkd_mngr/create";
		}
		uiModel.asMap().clear();
		redirectAttributes.addFlashAttribute("message",
				new Message("success", messageSource.getMessage("bar_save_success", new Object[] {}, locale)));

		// Persist new Market data
		barService.saveMarketData(marketData);
		logger.info("Market data: " + marketData.getId());

		return "redirect:/mkd_mngr/" + UrlUtil.encodeUrlPathSegment(marketData.getId().toString(), httpServletRequest);
	}

	// Update
	@RequestMapping(value = "/{id}", params = "form", method = RequestMethod.POST)
	public String update(@Valid Bar marketData, BindingResult bindingResult, Model uiModel,
			HttpServletRequest httpServletRequest, RedirectAttributes redirectAttributes, Locale locale) {
		logger.info("Updating a market data");
		if (bindingResult.hasErrors()) {
			uiModel.addAttribute("message",
					new Message("error", messageSource.getMessage("bar_save_fail", new Object[] {}, locale)));
			uiModel.addAttribute("marketData", marketData);
			return "mkd_mngr/update";
		}
		uiModel.asMap().clear();
		redirectAttributes.addFlashAttribute("message",
				new Message("success", messageSource.getMessage("bar_save_success", new Object[] {}, locale)));

		barService.updateMarketData(marketData);
		return "redirect:/mkd_mngr/" + UrlUtil.encodeUrlPathSegment(marketData.getId().toString(), httpServletRequest);
	}
	
	@ModelAttribute("stockSymbols")
	public List<WatchListDesc> initializeWatchLists(){
		return watchListService.findAll();
	}
	
/*	@ModelAttribute("instrumentSymbols")
	public List<Instrument> initializeInstrument(){
		return instrumentService.findAll();
	}*/

/*	@RequestMapping(method = RequestMethod.GET) //
	public String viewAccount(ModelMap model, @RequestParam String stockSymbol) {
		List<Bar> barList = service.getBarList(stockSymbol);
		model.addAttribute("barData", barList);
		return "view-data";
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public String searchGet1() {
		return "search";
	}

	@RequestMapping(value = "/DataController", method = RequestMethod.GET)
	public String searchGet2(ModelMap model, @RequestParam String searchParam) {
		List<String> tickets = service.searchTickersByChars(searchParam);
		model.addAttribute("THE_SEARCH_RESULT_LIST", tickets);
		return "search";
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public String searchPost(ModelMap model, @RequestParam String stockSymbol) {
		List<Bar> barList = service.getBarList(stockSymbol);
		model.addAttribute("barData", barList);
		return "view-data";
	}*/
}
