package com.gjj.igden.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gjj.igden.model.MarketData;

public interface MarketDataRepository extends JpaRepository<MarketData, Integer>{

}
