package com.gjj.igden.grid;

import java.util.List;
import com.gjj.igden.model.MarketData;

/**
 * @author Abolaji Salau
 *
 */
public class MarketDataGrid {

	private int totalPages;
	
	private int currentPage;
	
	private long totalRecords;
	
	private List<MarketData> marketData;

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public long getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(long totalRecords) {
		this.totalRecords = totalRecords;
	}

	public List<MarketData> getMeterReadData() {
		return marketData;
	}

	public void setMarketData(List<MarketData> marketData) {
		this.marketData = marketData;
	}
	
}
