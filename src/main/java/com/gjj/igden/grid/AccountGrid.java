package com.gjj.igden.grid;

import java.util.List;

import com.gjj.igden.model.Account;

/**
 * @author Abolaji Salau
 *
 */
public class AccountGrid {

	private int totalPages;
	
	private int currentPage;
	
	private long totalRecords;
	
	private List<Account> accountData;

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public long getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(long totalRecords) {
		this.totalRecords = totalRecords;
	}

	public List<Account> getAccountData() {
		return accountData;
	}

	public void setAccountData(List<Account> personData) {
		this.accountData = personData;
	}
	
}
