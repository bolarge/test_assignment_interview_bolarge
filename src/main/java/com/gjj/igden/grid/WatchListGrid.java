package com.gjj.igden.grid;

import java.util.List;

import com.gjj.igden.model.WatchListDesc;

/**
 * @author Abolaji Salau
 *
 */
public class WatchListGrid {

	private int totalPages;
	
	private int currentPage;
	
	private long totalRecords;
	
	private List<WatchListDesc> watchListData;

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public long getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(long totalRecords) {
		this.totalRecords = totalRecords;
	}

	public List<WatchListDesc> getPersonData() {
		return watchListData;
	}

	public void setPersonData(List<WatchListDesc> watchListData) {
		this.watchListData = watchListData;
	}
	
}
