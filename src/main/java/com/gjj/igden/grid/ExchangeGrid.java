package com.gjj.igden.grid;

import java.util.List;

import com.gjj.igden.model.Exchange;

/**
 * @author Abolaji Salau
 *
 */
public class ExchangeGrid {

	private int totalPages;
	
	private int currentPage;
	
	private long totalRecords;
	
	private List<Exchange> exchangeData;

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public long getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(long totalRecords) {
		this.totalRecords = totalRecords;
	}

	public List<Exchange> getExchangeData() {
		return exchangeData;
	}

	public void setExchangeData(List<Exchange> exchangeData) {
		this.exchangeData = exchangeData;
	}
	
}
