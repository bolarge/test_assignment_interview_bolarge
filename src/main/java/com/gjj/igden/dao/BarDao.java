package com.gjj.igden.dao;

import java.util.List;

import com.gjj.igden.model.Bar;

public interface BarDao { //extends JpaRepository<Bar, BarSetId> {

	public Bar findById(Long id);
	public void save(Bar bar);
	public void remove(Bar bar);
	public List<Bar> findAll();

  /*@Query("Select b from Bar b where b.id.instId = :instId")
  public List<Bar> findByInstId(@Param("instId") String instId);

  @Query("SELECT DISTINCT b.id.instId FROM Bar b WHERE b.id.instId LIKE %:tickerNamePart%")
  public List<Object> findByTicketName(@Param("tickerNamePart") String ticketNamePart);

  @Query("Select b from Bar b where b.ticker = :ticker")
  public List<Bar> findByTickerName(@Param("ticker") String ticker);*/	
}
