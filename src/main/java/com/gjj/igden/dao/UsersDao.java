package com.gjj.igden.dao;

import java.util.List;

import com.gjj.igden.model.Users;

public interface UsersDao {

	Users findById(Long id);
	
	Users findByEmail(String email);
	
	Users findByName(String name);
	
	void save(Users user);
	
	List<Users> findAll();
    
}
