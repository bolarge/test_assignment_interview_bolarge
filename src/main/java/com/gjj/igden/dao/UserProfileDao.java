package com.gjj.igden.dao;

import java.util.List;

import com.gjj.igden.model.UserProfile;


public interface UserProfileDao {

	List<UserProfile> findAll();
	
	UserProfile findByType(String type);
	
	UserProfile findById(int id);
}
