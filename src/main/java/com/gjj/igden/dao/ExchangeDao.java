package com.gjj.igden.dao;

import java.util.List;

import com.gjj.igden.model.Exchange;

public interface ExchangeDao {

	public Exchange findById(Integer id);
	public void save(Exchange exchange);
	public void remove(Exchange exchange);
	public List<Exchange> findAll();
	
}
