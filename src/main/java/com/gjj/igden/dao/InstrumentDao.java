package com.gjj.igden.dao;

import java.util.List;

import com.gjj.igden.model.Instrument;

public interface InstrumentDao {
	
	public Instrument findBySymbol(String symbol);
	public void save(Instrument instrument);
	public void remove(Instrument instrument);
	public List<Instrument> findAll();
}
