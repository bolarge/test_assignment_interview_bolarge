package com.gjj.igden.dao;

import com.gjj.igden.model.Account;
import java.util.List;

public interface AccountDao {
	
	public Account findById(Long id);
	public Account findByEmail(String email);
	public void save(Account account);
	public void remove(Account account);	
	List<Account> findAllAccounts();
	
/*	boolean delete(Account account); // Y return  boolean (em give status) Insert or update a account	public Account save(Account account);
	boolean delete(int id);
	Account getAccountById(int id);
	boolean update(Account acc);
	boolean create(Account account);
	boolean setImage(int accId, InputStream is);
	byte[] getImage(int accId);
	void setDataSource(DataSource dataSource);
	void setNamedParamJbd(NamedParameterJdbcTemplate namedParamJbd);*/
	
	//public Account findByAccountName(String accountName);
	//public List<Account> findAll(); 			
}
