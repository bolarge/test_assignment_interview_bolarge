package com.gjj.igden.dao.daoImpl;

import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gjj.igden.dao.AccountDao;
import com.gjj.igden.model.Account;

@Repository("jpaAccountDao")
@Transactional
public class AccountDaoImpl implements AccountDao {

    @PersistenceContext
    protected EntityManager entityManager;    
    private Log log = LogFactory.getLog(AccountDaoImpl.class);

    public List<Account> getAllAccounts() {
        List<Account> list = entityManager.createNamedQuery("select * from account", Account.class).getResultList();
        System.out.println("Size of list " + list.size());
        return list; //entityManager.createQuery("select a from Account a", Account.class).getResultList();
    }
    
    public Account findByEmail(String email) {
		return entityManager.find(Account.class, email);
	}
    
    public Account findByAccountName(String accountName) {
		return entityManager.find(Account.class, accountName);
	}
        
    @Transactional(readOnly=true)
	public Account findById(Integer id) {
		TypedQuery<Account> query = entityManager.createNamedQuery("Account.findById", Account.class);
		query.setParameter("id", id);
		return query.getSingleResult();
	}
    
    @Override
	public Account save(Account account) {
    	if (account.getId() == null) { // Insert Account
        	DateFormat df = new SimpleDateFormat("dd/MM/yy");
            Date dateobj = new Date();
            String creationDate = df.format(dateobj);
            account.setCreationDate(creationDate);
    		log.info("Inserting new account");
    		entityManager.persist(account);
    	} else {                       // Update Contact
    		entityManager.merge(account);
    		log.info("Updating existing account");
    	}
    		log.info("Account saved with id: " + account.getId());
    	return account;
	}

	@Override
	public void remove(Account account) {
		Account mergedAccount = entityManager.merge(account);
		entityManager.remove(mergedAccount);
		log.info("Account with id: " + account.getId() + " deleted successfully");
		
	}
	
	
	
	
	
	//@Transactional
    public boolean delete(Account account) {
        boolean delete = false;
        try {
            Account a = entityManager.merge(account);
            entityManager.remove(a);
            delete = true;
        } catch (Exception e) {
            delete = false;
            e.printStackTrace();
        }
        return delete;
    }

    //@Transactional
    public boolean delete(int id) {
        boolean delete = false;
        try {
            Account account = entityManager.find(Account.class, id);
            Account a = entityManager.merge(account);
            entityManager.remove(a);
            delete = true;
        } catch (Exception e) {
            delete = false;
            e.printStackTrace();
        }
        return  delete;
    }

    @Transactional(readOnly=true)
    public Account getAccountById(int id) {
        return entityManager.find(Account.class, id);
    }
    
    //@Transactional
    public boolean update(Account acc) {
        boolean update = false;
        try {
            entityManager.merge(acc);
            update = true;
        } catch (Exception e) {
            e.printStackTrace();
            update=false;
        }
        return  update;
    }

    //@Transactional
    public boolean create(Account account) {
        boolean create = false;
        try {
            DateFormat df = new SimpleDateFormat("dd/MM/yy");
            Date dateobj = new Date();
            String creationDate = df.format(dateobj);
            account.setCreationDate(creationDate);
            entityManager.persist(account);
            create = true;
        } catch (Exception e) {
            e.printStackTrace();
            create = false;
        }
        return  create;
    }

    //@Transactional
    public boolean setImage(int accId, InputStream is) {
        boolean image = false;
        try {
            Account account = entityManager.find(Account.class,accId);
            account.setImage(IOUtils.toByteArray(is));
            entityManager.merge(account);
            image = true;
        } catch (Exception e) {
            e.printStackTrace();
            image=false;
        }
        return image;
    }

    @Override
    public byte[] getImage(int accId) {
        //getAccountById(accId);
       /* MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("accId", accId, Types.INTEGER);
        String sqlQuery = "SELECT image from account WHERE account_id = :accId";
        byte[] bytes = namedParamJbd.query(sqlQuery, parameters, new AccountByteArraysRowMapper()).get(0);*/
        Account account = entityManager.find(Account.class, accId);
        return account.getImage();
    }
}
