package com.gjj.igden.dao.daoImpl;

import java.util.List;

import javax.persistence.TypedQuery;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gjj.igden.dao.AbstractDao;
import com.gjj.igden.dao.ExchangeDao;
import com.gjj.igden.model.Exchange;

@Repository("exchangeDao")
@Transactional
public class ExchangeDaoImpl extends AbstractDao<Integer, Exchange>implements ExchangeDao {
	    
    private Log log = LogFactory.getLog(ExchangeDaoImpl.class);

	@Transactional(readOnly=true)
	public List<Exchange> findAll() {
		List<Exchange> exchange = getEntityManager().createNamedQuery("Exchange.findAll", Exchange.class).getResultList();
		return exchange;
	}
    
    @Transactional(readOnly=true)
	public Exchange findByExchangeId(String exchangeId) {
    	TypedQuery<Exchange> query = getEntityManager().createNamedQuery("Exchange.findByExchangeId", Exchange.class);
		query.setParameter("exchangeId", exchangeId);
		return query.getSingleResult();
	}

	@Override
	public void save(Exchange exchange) {
		if (exchange.getExchangeId() == null) { 
			log.info("Inserting new Exchange");
			getEntityManager().persist(exchange);
		} else {               
			getEntityManager().merge(exchange);
			log.info("Updating existing Exchange");
		}
		log.info("Exchange saved with id: " + exchange.getExchangeId());
	}

	public void remove(Exchange exchange) {
		Exchange mergedExchange= getEntityManager().merge(exchange);
		getEntityManager().remove(mergedExchange);
		log.info("Exchange with id: " + exchange.getExchangeId() + " deleted successfully");
		
	}

	@Transactional(readOnly=true)
	public Exchange findById(Integer id) {
		Exchange exchange = getByKey(id);
		return exchange;
	}

}
