package com.gjj.igden.dao.daoImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.gjj.igden.dao.AbstractDao;
import com.gjj.igden.dao.BarDao;
import com.gjj.igden.model.Bar;

@Repository("barDao") //@Component
@Transactional
public class BarDaoImpl extends AbstractDao<Long, Bar> implements BarDao{
	
	private Log log = LogFactory.getLog(BarDaoImpl.class);

	@Override
	public Bar findById(Long id) {
		Bar bar = getByKey(id);
		if(bar!=null){
			//initializeCollection(account.getWatchLists());
			log.info("Bar :" + bar.toString());
		}
		//log.info("Bar :" + bar.toString());
		return bar;
	}

	@Override
	public void save(Bar bar) {
		persist(bar);
	}

	@Override
	public void remove(Bar bar) {
		// TODO Auto-generated method stub
		
	}

	@SuppressWarnings("unchecked")
	public List<Bar> findAll() {
		List<Bar> bars = getEntityManager()
				.createQuery("SELECT b FROM Bar b ORDER BY b.marketDataId ASC")
				.getResultList();
		log.info("There are :" + bars.size());
		return bars;
	}
	
	
	/*//INITIAL IMPLEMENTATION WITH SPRING DATA & JPA WITH HIBERNATE AS PERSISTENCE ENGINE
	
	@PersistenceContext
	private EntityManager em;    
    private Log log = LogFactory.getLog(BarDaoImpl.class);

	@Transactional(readOnly=true)
	public List<Bar> findAll() {
		List<Bar> bar = em.createNamedQuery("Bar.findAll", Bar.class).getResultList();
		return bar;
	}
    
    @Transactional(readOnly=true)
	public Bar findById(Long  id) {
    	TypedQuery<Bar> query = em.createNamedQuery("Bar.findByMarketId", Bar.class);
		query.setParameter("id", id);
		return query.getSingleResult();
	}

	public Bar save(Bar bar) {
		if (bar.getMarketDataId() == null) { 
			log.info("Inserting new Bar");
			em.persist(bar);
		} else {                    
			em.merge(bar);
			log.info("Updating existing watchListDesc");
		}
		log.info("Bar saved with id: " + bar.getMarketDataId());
		return bar;
	}

	public void remove(Bar bar) {
		Bar mergedBar= em.merge(bar);
		em.remove(mergedBar);
		log.info("Bar with id: " + bar.getMarketDataId() + " deleted successfully");	
	}*/

	
	
	
	///@Resource
  //BarDao barDao;

  /*@Transactional
  public Bar getSingleBar(long md_id, String instId) {
    BarSetId barSetId = new BarSetId(md_id, instId);
    return barDao.findOne(barSetId);
  }

  public List<Bar> getBarList(String instId) {
    List<Bar> resultList = barDao.findByInstId(instId);
    System.out.print(resultList.size());
    return  resultList;
  }

  @Transactional
  public boolean createBar(Bar bar) throws DaoException {
    boolean created = false;
    try {
      barDao.save(bar);
      created=true;
    } catch (Exception e) {
      e.printStackTrace();
      created=false;
    }
    return  created;
  }*/

  /*@Transactional
  public boolean updateBar(Bar bar) {
    boolean update = false;
    try {
        barDao.saveAndFlush(bar);
        //entityManager.merge(bar);
        update = true;
    } catch (Exception e) {
      e.printStackTrace();
      update = false;
    }
    return update;
  }

  @Transactional
  public boolean deleteBar(long mdId, String instId) {
      boolean delete = false;
     try {
         barDao.delete(new BarSetId(mdId, instId));
         delete = true;
     } catch(Exception e) {
        e.printStackTrace();
         delete = false;

     }
      return delete;
  }

  @Transactional
  public boolean deleteBar(Bar bar) {
    return deleteBar(bar.getId().getId(), bar.getId().getInstId());
  }

  public List<String> searchTickersByChars(String tickerNamePart) {

    List<Object> list =  barDao.findByTicketName(tickerNamePart);
    List<String>  l = new ArrayList<String>();
    if(list != null && list.size()>0) {
      for(Object o : list) {
        String s = (String) o;
        l.add(s);
      }
    }
    return l;
    Map<String, Object> parameters = new HashMap<>();
    parameters.put("searchParam", tickerNamePart + "%");
    String sqlQuery = "	SELECT DISTINCT instId_fk FROM market_data WHERE instId_fk LIKE  " +
      " :searchParam ";
    return namedParamJbd.query(sqlQuery, parameters, new MarketDataRowMapper());
  }

  public List<Bar> findByTicker(String ticker) {
    return barDao.findByTickerName(ticker);
  }
*/

}
