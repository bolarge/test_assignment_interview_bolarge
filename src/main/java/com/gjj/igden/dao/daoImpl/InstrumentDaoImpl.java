package com.gjj.igden.dao.daoImpl;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.gjj.igden.dao.AbstractDao;
import com.gjj.igden.dao.InstrumentDao;
import com.gjj.igden.model.Account;
import com.gjj.igden.model.Instrument;

@Repository("instrumentDao")
@Transactional
public class InstrumentDaoImpl extends AbstractDao<String, Instrument> implements InstrumentDao{
    
    private Log log = LogFactory.getLog(InstrumentDaoImpl.class);

    //@SuppressWarnings("unchecked")
	public List<Instrument> findAll() {
		List<Instrument> instruments = getEntityManager().createNamedQuery("Instrument.findAll", Instrument.class).getResultList();
		return instruments;
		/*List<Instrument> instruments = getEntityManager()
				.createQuery("SELECT i FROM Instrument i ORDER BY i.symbol ASC")
				.getResultList();
		log.info("There are :" + instruments.size());
		return instruments;*/
	}
    
	public Instrument findBySymbol(String symbol) {
    	TypedQuery<Instrument> query = getEntityManager().createNamedQuery("Instrument.findBySymbol", Instrument.class);
		query.setParameter("symbol", symbol);
		return query.getSingleResult();
	}

	public void save(Instrument instrument) {
		if (instrument.getSymbol() == null) { 
			log.info("Inserting new instrument");
			getEntityManager().persist(instrument);
		} else {               
			getEntityManager().merge(instrument);
			log.info("Updating existing instrument");
		}
		log.info("Instrument saved with id: " + instrument.getSymbol());
		//return instrument;
	}

	public void remove(Instrument instrument) {
		Instrument mergedExchange= getEntityManager().merge(instrument);
		getEntityManager().remove(mergedExchange);
		log.info("Instrument with id: " + instrument.getSymbol() + " deleted successfully");		
	}

}
