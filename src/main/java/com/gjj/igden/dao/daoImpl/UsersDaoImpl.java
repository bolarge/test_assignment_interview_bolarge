package com.gjj.igden.dao.daoImpl;

import java.util.List;

import javax.persistence.NoResultException;
import javax.transaction.Transactional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.gjj.igden.dao.AbstractDao;
import com.gjj.igden.dao.UsersDao;
import com.gjj.igden.model.Users;

@Repository("usersDao")
@Transactional
public class UsersDaoImpl extends AbstractDao<Long, Users> implements UsersDao{
	
	private final Log log = LogFactory.getLog(UsersDaoImpl.class);

	public Users findById(Long id) {
		Users user = getByKey(id);
		if (user != null) {
			initializeCollection(user.getUserProfiles());
		}
		return user;
	}

	public Users findByEmail(String email) {
		log.info("Email : " + email);
		try {
			Users user = (Users) getEntityManager().createQuery("SELECT u FROM Users u WHERE u.email LIKE :email")
					.setParameter("email", email).getSingleResult();
			if (user != null) {
				initializeCollection(user.getUserProfiles());
			}
			return user;
		} catch (NoResultException ex) {
			return null;
		}
	}

	public Users findByName(String name) {
		log.info("Name : " + name);
		try {
			Users user = (Users) getEntityManager().createQuery("SELECT u FROM Users u WHERE u.name LIKE :name")
				.setParameter("name", name).getSingleResult();
			if (user != null) {
				initializeCollection(user.getUserProfiles());
			}
			return user;
		} catch (NoResultException ex) {
			return null;
		}
	}

	public void save(Users user) {
		persist(user);		
	}

	@SuppressWarnings("unchecked")
	public List<Users> findAll() {
		List<Users> users = getEntityManager().createQuery("SELECT u FROM Users u ORDER BY u.email ASC")
				.getResultList();
		log.info("There are :" + users.size());
		return users;
	}

}
