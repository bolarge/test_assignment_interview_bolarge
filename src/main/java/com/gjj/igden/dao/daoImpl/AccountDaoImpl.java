package com.gjj.igden.dao.daoImpl;

import java.util.List;

import javax.persistence.NoResultException;
import javax.transaction.Transactional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.gjj.igden.dao.AbstractDao;
import com.gjj.igden.dao.AccountDao;
import com.gjj.igden.model.Account;

@Repository("accountDao")
@Transactional
public class AccountDaoImpl extends AbstractDao<Long, Account> implements AccountDao {
	
	private final Log log = LogFactory.getLog(AccountDaoImpl.class);

	public Account findById(Long id) {
		Account account = getByKey(id);
		if(account!=null){
			initializeCollection(account.getUserProfiles());
			initializeCollection(account.getWatchLists());
		}
		log.info("Account :" + account.toString());
		return account;
	}

	public Account findByEmail(String email) {
		log.info("Email : "+email);
		try{
			Account account = (Account) getEntityManager()
					.createQuery("SELECT a FROM Account a WHERE u.email LIKE :email")
					.setParameter("email", email)
					.getSingleResult();
			
			if(account!=null){
				log.info("Account :" + account.toString());
				initializeCollection(account.getUserProfiles());
			}
			return account; 
		}catch(NoResultException ex){
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Account> findAllAccounts() {
		List<Account> accounts = getEntityManager()
				.createQuery("SELECT a FROM Account a ORDER BY a.accountName ASC")
				.getResultList();
		log.info("There are :" + accounts.size());
		return accounts;
	}

	public void save(Account account) {
		persist(account);
	}

	public void deleteByEmail(String email) {
		Account account = (Account ) getEntityManager()
				.createQuery("SELECT a FROM Account a WHERE a.email LIKE :email")
				.setParameter("email", email)
				.getSingleResult();
		delete(account);
	}

	@Override
	public void remove(Account account) {
		// TODO Auto-generated method stud
	}
}
