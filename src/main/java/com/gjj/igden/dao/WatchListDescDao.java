package com.gjj.igden.dao;

import java.util.List;

import com.gjj.igden.model.WatchListDesc;


public interface WatchListDescDao {//extends JpaRepository<WatchListDesc, DataSetId> {
	
	public WatchListDesc findById(Integer id);
	public void save(WatchListDesc wl);
	public void remove(WatchListDesc wl);	
	public List<WatchListDesc> findAll();

}
