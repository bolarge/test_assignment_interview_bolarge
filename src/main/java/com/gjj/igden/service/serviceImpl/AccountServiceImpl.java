package com.gjj.igden.service.serviceImpl;

import com.gjj.igden.dao.AccountDao;
import com.gjj.igden.model.Account;
import com.gjj.igden.service.AccountService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("accountService")
@Transactional
public class AccountServiceImpl implements AccountService{
	
	@Autowired
	private AccountDao accountDao;

	public List<Account> findAll() {
		return accountDao.findAllAccounts();
	}

	public Account findById(Long id) {
		return accountDao.findById(id);
	}

	public void saveAccount(Account account) {
		accountDao.save(account);
	}

	public void delete(Account account) {
		accountDao.remove(account);	
	}
	
	public Account findByEmail(String userName) {
		return accountDao.findByEmail(userName);
	}
	
	public Account findByAccountName(String accountName) {
		return accountDao.findByEmail(accountName);
	}

	public void updateAccount(Account account) {
		Account entity = accountDao.findById(account.getId());
		if(entity!=null){
			entity.setAccountName(account.getAccountName());
			entity.setEmail(account.getEmail());
			entity.setAdditionalInfo(account.getAdditionalInfo());
			entity.setCreationDate(account.getCreationDate());
			entity.setImage(account.getImage());
			entity.setUserProfiles(account.getUserProfiles());
			entity.setWatchLists(account.getWatchLists());
		}
		
	}

	/*public boolean createAccount(Account account) {
		boolean resultFlag = accountDao.create(account);
		if (resultFlag) {
			return true;
		} else {
			System.err.println(" something bad happen - account wasn't added ");
			return false;
		}
	}

	public boolean updateAccount(Account account) {
		return accountDao.update(account);
	}*/

	/*public Account retrieveAccount(int accId) {
		Account user = accountDao.getAccountById(accId);
		List<IWatchListDesc> dataSetList = watchListDescDao.getDataSetsAttachedToAcc(accId);
		user.setDataSets(dataSetList);
		return user;
	}

	public boolean delete(int id) {
		return accountDao.delete(id);
	}

	public boolean setImage(int accId, InputStream is) {
		return accountDao.setImage(accId, is);
	}*/

	/*public byte[] getImage(int accId) {
		return accountDao.getImage(accId);
	}*/

}
