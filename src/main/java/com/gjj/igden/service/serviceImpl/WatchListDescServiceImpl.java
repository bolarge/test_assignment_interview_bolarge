package com.gjj.igden.service.serviceImpl;

import com.gjj.igden.dao.WatchListDescDao;
import com.gjj.igden.model.WatchListDesc;
import com.gjj.igden.service.WatchListDescService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service("watchListDescService")
@Transactional
public class WatchListDescServiceImpl implements WatchListDescService{
  
	@Autowired
	private WatchListDescDao watchListDescDao;
	
	@Transactional(readOnly=true)
	public List<WatchListDesc> findAll() {
		return new ArrayList<WatchListDesc>(watchListDescDao.findAll());
	}

	@Transactional(readOnly=true)
	public WatchListDesc findById(Integer id) {
		return this.watchListDescDao.findById(id);
	}

	public void save(WatchListDesc aWatchList) {
		this.watchListDescDao.save(aWatchList);
	}

	public void delete(WatchListDesc aWatchList) {
		this.watchListDescDao.remove(aWatchList);
	}

	/*
	public List<IWatchListDesc> getDataSetsAttachedToAcc(int id) {
		return watchListDescDao.getDataSetsAttachedToAcc(id);
	}

	public List<String> getStockSymbolsList(int id) {
		return watchListDescDao.getAllStockSymbols(id);
	}

	public boolean delete(int accId, int watchListId) {
		return watchListDescDao.deleteWatchListDesc(watchListId, accId);
	}

	public boolean delete(IWatchListDesc watchListDesc) {
		return watchListDescDao.deleteWatchListDesc(watchListDesc);
	}

	public boolean create(IWatchListDesc watchListDesc) {
		watchListDesc.setStockSymbolsListFromOperationList(watchListDesc.getOperationParameterses());
		return watchListDescDao.createWatchListDesc(watchListDesc);
	}

	public IWatchListDesc getWatchListDesc(int dsId, int accId) {
		return watchListDescDao.getWatchListDesc(dsId, accId);
	}

	public boolean update(IWatchListDesc watchListDesc) {
		return watchListDescDao.updateWatchListDesc(watchListDesc);
	}
	*/
}
