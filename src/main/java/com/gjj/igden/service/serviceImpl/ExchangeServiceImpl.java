package com.gjj.igden.service.serviceImpl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gjj.igden.dao.ExchangeDao;
import com.gjj.igden.model.Exchange;
import com.gjj.igden.service.ExchangeService;

@Service("exchangeService")
@Transactional
public class ExchangeServiceImpl implements ExchangeService{

	private ExchangeDao exchangeDao;
	
	public List<Exchange> findAll() {
		return exchangeDao.findAll();
	}

	public Exchange findById(Integer id) {
		return exchangeDao.findById(id);
	}

	public void saveExchange(Exchange exchange) {
		exchangeDao.save(exchange);		
	}

	public void updateExchange(Exchange exchange) {
		exchangeDao.save(exchange);	
	}

	public void delete(Exchange exchange) {
		exchangeDao.remove(exchange);		
	}

	public Exchange findByDataSet(int dataSet) {
		return null;
	}

	@Override
	public Exchange findByAccountName(String accountName) {
		return null;
	}

}
