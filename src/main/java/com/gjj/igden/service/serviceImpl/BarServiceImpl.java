package com.gjj.igden.service.serviceImpl;

import com.gjj.igden.model.Bar;
import com.gjj.igden.service.BarService;
import com.google.common.collect.Lists;
import com.gjj.igden.dao.BarDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("barService")
@Transactional
public class BarServiceImpl implements BarService{
	
  @Autowired
  private BarDao barDao;
  
  public void updateMarketData(Bar bar) {
		
		Bar entity = barDao.findById(bar.getId());
		if(entity!=null){
			entity.setMarketDataId(bar.getMarketDataId());
			entity.setTicker(bar.getTicker());
			entity.setOpen(bar.getOpen());
		}
	}

	public void saveMarketData(Bar bar) {
		barDao.save(bar);
	}

	public void deleteBar(Bar bar) {
		this.barDao.remove(bar);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Bar> findAll() {
		return Lists.newArrayList(this.barDao.findAll());
	}
	
	@Transactional(readOnly=true)
	public Bar findById(Long id) {
		return this.barDao.findById(id);
	}

	@Override
	public void delete(Bar bar) {
		// TODO Auto-generated method stub
	}

	@Override
	public Bar findByDataSet(int dataSet) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Bar findByAccountName(String accountName) {
		// TODO Auto-generated method stub
		return null;
	}

  /*public List<Bar> getBarList(String instId) { //This should be findAll with their relations
    return barDao.getBarList(instId);
  }
  														//One bar has any instrument 1:*
  public Bar getSingleBar(long barId, String instId) {  //if you have the ID of the bar, it all you need to fetch it and anything elation to it
    return barDao.getSingleBar(barId, instId);
  }*/
  

  /*public List<String> searchTickersByChars(String tickerNamePart) {
    return barDao.searchTickersByChars(tickerNamePart);
  }*/
}
