package com.gjj.igden.service.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gjj.igden.dao.AbstractDao;
import com.gjj.igden.dao.UsersDao;
import com.gjj.igden.model.Users;

import com.gjj.igden.service.UsersService;
	
@Service("UsersService")
@Transactional
public class UsersServiceImpl extends AbstractDao<Long, Users> implements UsersService {
	
	@Autowired
	private UsersDao usersDao;

	public Users findById(Long id) {
		return usersDao.findById(id);
	}

	public Users findByName(String name) {
		return usersDao.findByName(name);
	}
	
	public Users findByEmail(String email) {
		return usersDao.findByEmail(email);
	}

	public void saveUsers(Users user) {
		usersDao.save(user);
	}

	public void updateUsers(Users user) {
		Users entity = usersDao.findById(user.getId());
		if(entity!=null){
			entity.setName(user.getName());
			entity.setEmail(user.getEmail());
			entity.setPassword(user.getPassword());
			entity.setPhone(user.getPhone());
			entity.setUserProfiles(user.getUserProfiles());
			entity.setDob(user.getDob());
		}
	}

	public void deleteUsersById(Long id) {
		// TO DO UsersRepository.delete(id);
	}

	public void deleteAllUserss() {
		// TO DO UsersRepository.deleteAll();
	}

	public List<Users> findAllUsers() {
		return usersDao.findAll();
	}

	public boolean isUsersExist(Users Users) {
		return findByName(Users.getName()) != null;
	}

}
