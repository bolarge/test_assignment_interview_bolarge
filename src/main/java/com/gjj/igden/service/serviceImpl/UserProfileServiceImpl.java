package com.gjj.igden.service.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gjj.igden.dao.UserProfileDao;
import com.gjj.igden.model.UserProfile;
import com.gjj.igden.service.UserProfileService;


@Service("userProfileService")
@Transactional
public class UserProfileServiceImpl implements UserProfileService{
	
	@Autowired
	private UserProfileDao dao;
	
	@Override
	public UserProfile findById(int id) {
		return dao.findById(id);
	}

	@Override
	public UserProfile findByType(String type){
		return dao.findByType(type);
	}
	
	@Override
	public List<UserProfile> findAll() {
		return dao.findAll();
	}
	
	public void save(UserProfile role) {
		// TODO Auto-generated method stub
		
	}
}
