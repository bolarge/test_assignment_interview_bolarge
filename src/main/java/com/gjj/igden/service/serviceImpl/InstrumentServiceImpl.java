package com.gjj.igden.service.serviceImpl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gjj.igden.dao.InstrumentDao;
import com.gjj.igden.model.Instrument;
import com.gjj.igden.service.InstrumentService;

@Service("instrumentService")
@Transactional
public class InstrumentServiceImpl implements InstrumentService{
	
	private InstrumentDao instrumentDao;
	
	public List<Instrument> findAll() {
		return instrumentDao.findAll();
	}

	public Instrument findBySymbol(String id) {
		return instrumentDao.findBySymbol(id);
	}

	public void saveInstrument(Instrument ins) {
		instrumentDao.save(ins);		
	}

	public void updateInstrument(Instrument ins) {
		instrumentDao.save(ins);
	}

	public void delete(Instrument ins) {
		instrumentDao.remove(ins);		
	}

	@Override
	public Instrument findByDataSet(int dataSet) {
		// TODO Auto-generated method stub
		return null;
	}

}
