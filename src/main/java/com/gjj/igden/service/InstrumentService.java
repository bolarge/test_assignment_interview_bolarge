package com.gjj.igden.service;

import java.util.List;

import com.gjj.igden.model.Instrument;

public interface InstrumentService {
	
	public List<Instrument> findAll();

	public Instrument findBySymbol(String id);

	public void saveInstrument(Instrument ins);
	
	public void updateInstrument(Instrument ins);

	public void delete(Instrument ins);
	
	public Instrument findByDataSet(int dataSet);
	
}
