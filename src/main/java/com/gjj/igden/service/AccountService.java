package com.gjj.igden.service;

import java.util.List;

import com.gjj.igden.model.Account;

public interface AccountService {

	public List<Account> findAll();

	public Account findById(Long id);
	
	public Account findByEmail(String email);
	
	public Account findByAccountName(String accountName);

	public void saveAccount(Account account);

	public void delete(Account account);
	
	public void updateAccount(Account account);

	//public Page<Account> findAllByPage(PageRequest pageRequest);

	
	
}
