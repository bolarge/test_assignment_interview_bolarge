package com.gjj.igden.service;

import java.util.List;

import com.gjj.igden.model.OperationParameters;

public interface Watchable {

	int getWatchListId();

	Integer getAccountId();

	int getMarketDataFrequency();

	List<String> getStockSymbolsList();

	void setStockSymbolsList(List<String> stockSymbolsList);

	void setStockSymbolsListFromOperationList(List<OperationParameters> stockSymbolsList);

	void setWatchListId(int watchListId);

	void setAccountId(int accountId);

	void setWatchListName(String watchListName);

	void setWatchListDetails(String watchListDetails);

	void setMarketDataFrequency(int marketDataFrequency);

	void setDataProviders(String dataProviders);

	void setOperationParameterses(List<OperationParameters> operationParameterses);

	String getWatchListName();

	String getWatchListDetails();

	String getDataProviders();

	String toString();

	List<OperationParameters> getOperationParameterses();

}
