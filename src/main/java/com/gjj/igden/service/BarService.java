package com.gjj.igden.service;

import java.util.List;

import com.gjj.igden.model.Bar;

public interface BarService {

	public List<Bar> findAll();

	public Bar findById(Long id);

	public void saveMarketData(Bar bar);
	
	public void updateMarketData(Bar bar);

	public void delete(Bar bar);
	
	public Bar findByDataSet(int dataSet);
	
	public Bar findByAccountName(String accountName);

	//public Page<Bar> findAllByPage(PageRequest pageRequest);

}
