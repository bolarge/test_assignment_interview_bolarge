package com.gjj.igden.service;

import java.util.List;

import com.gjj.igden.model.WatchListDesc;

public interface WatchListDescService {

	public List<WatchListDesc> findAll();

	public WatchListDesc findById(Integer id);

	public void save(WatchListDesc aWatchList);

	public void delete(WatchListDesc aWatchList);

	//public Page<WatchListDesc> findAllByPage(PageRequest pageRequest);
}
