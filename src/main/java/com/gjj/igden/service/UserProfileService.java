package com.gjj.igden.service;

import java.util.List;

import com.gjj.igden.model.UserProfile;


public interface UserProfileService {

	UserProfile findById(int id);

	UserProfile findByType(String type);
	
	List<UserProfile> findAll();

	void save(UserProfile role);
	
}
