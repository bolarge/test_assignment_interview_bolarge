package com.gjj.igden.service;

import java.util.List;

import com.gjj.igden.model.Users;

public interface UsersService {

	Users findById(Long id);

	Users findByName(String name);
	
	Users findByEmail(String email);

	void saveUsers(Users Users);

	void updateUsers(Users Users);

	void deleteUsersById(Long id);

	void deleteAllUserss();

	List<Users> findAllUsers();

	boolean isUsersExist(Users Users);

}
