package com.gjj.igden.service;

import java.util.List;

import com.gjj.igden.model.Exchange;

public interface ExchangeService {
	
	public List<Exchange> findAll();

	public Exchange findById(Integer id);

	public void saveExchange(Exchange xch);
	
	public void updateExchange(Exchange xch);

	public void delete(Exchange xch);
	
	public Exchange findByDataSet(int dataSet);
	
	public Exchange findByAccountName(String accountName);
}
