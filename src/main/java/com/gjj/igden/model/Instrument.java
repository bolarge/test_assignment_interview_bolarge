package com.gjj.igden.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * Instrument identifier
 */
@SuppressWarnings("serial")
@Table(name="INSTRUMENT")
@Entity
@NamedQueries({
	@NamedQuery(name="Instrument.findAll",
			    query="select i from Instrument i"),
	@NamedQuery(name="Instrument.findBySymbol",
	query="select i from Instrument i where i.symbol = :symbol")
	})
@SqlResultSetMapping(
		name="instrumentResult",
		entities=@EntityResult(entityClass=Instrument.class))
public class Instrument implements Serializable{
	
	@Id @Column(name = "SYMBOL", unique = true, nullable = false)
	private String symbol;
	
	@Version
	@Column(name="VERSION")
	private int version;
	//protected String exchangeId; No Longer Needed
	//Relations
	/*@ManyToOne
	@JoinColumn(name = "EXCHANGE_ID")
	private Exchange exchange; //Assuming more than one Instrument is been watched in an exchange then 1:* from Instrument to Exchange 
	*/
	//Constructor
	public Instrument() {}
	
	public Instrument(String str) {
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	
	public int getVersion() {
		return version;
	}
	
	public void setVersion(int version) {
		this.version = version;
	}

	/*public Exchange getExchange() {
		return this.exchange;
	}
	
	public void setExchanges(Exchange exchange) {
		this.exchange = exchange;
	}*/
	
	@Override
	public String toString() {
		return "Instrument [symbol=" + symbol + ", version=" + version + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((symbol == null) ? 0 : symbol.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Instrument))
			return false;
		Instrument other = (Instrument) obj;
		if (symbol == null) {
			if (other.symbol != null)
				return false;
		} else if (!symbol.equals(other.symbol))
			return false;
		return true;
	}

	

	

	/*@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof InstId)) {
			return false;
		}
		InstId altInstId = (InstId) o;
		return Objects.equal(this.getSymbol(), altInstId.symbol) && Objects.equal(getId(), altInstId.getId());
	}*/

	/*public Exchange getExchange(String exchangeId) {
		if (exchangeId.equalsIgnoreCase("NYSE")) {
			return Exchange.NYSE;
		} else if (exchangeId.equalsIgnoreCase("nasdaq")) {
			return Exchange.NASDAQ;
		} else {
			throw new IllegalArgumentException("your exchange id is not correct");
		}
	}*/

	/*@Override
	public int hashCode() {
		return Objects.hashCode(this.getSymbol(), this.getId());
	}

	@Override
	public String toString() {
		return this.getSymbol() + SEPARATOR + this.getId();
	}*/
}
