package com.gjj.igden.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.TimeZone;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@SuppressWarnings("serial")
@Table(name="EXCHANGE")
@Entity
@NamedQueries({
	@NamedQuery(name="Exchange.findAll",
			    query="select e from Exchange e"), 
	@NamedQuery(name="Exchange.findByExchangeId",
				query="SELECT e from Exchange e WHERE e.id = :exchangeId"),
	@NamedQuery(name="Exchange.findByName",
				query="SELECT e from Exchange e WHERE e.name = :name")
	})
@SqlResultSetMapping(
		name="exchangeResult",
		entities=@EntityResult(entityClass=Exchange.class))
public class Exchange implements Serializable{

	//final static TimeZone DEFAULT_TIME_ZONE = TimeZone.getTimeZone("America/New_York");	
	//Why Hard code values
	/*public static Exchange NASDAQ = new Exchange("NASDAQ",
			"National Association of Securities Dealers Automated Quotations", "9:30 a.m.", "4:00 p.m.",
			DEFAULT_TIME_ZONE); // UTC-4 - 70% of the year time
	
	public static Exchange NYSE = new Exchange("NYSE", "New York Stock Exchange", "9:30 a.m.", "4:00 p.m.",
			DEFAULT_TIME_ZONE); // // UTC-4 - - 70% of the year time
	 */	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "ID")
	private Integer id;
	
	@Column(name = "EXCHANGE_ID")
	private String exchangeId;
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "HOURS_FROM")
	private String markerHoursFrom;
	
	@Column(name = "HOURS_TO")
	private String marketHoursTo;
	
	@Column(name = "TIMEZONE")
	private TimeZone timeZone;
	
	@Version
	@Column(name="VERSION")
	private int version;
	
	//@NotEmpty
	//@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "EXCHANGE_INSTRUMENT_REL", 
			joinColumns = @JoinColumn(name = "EXCHANGE_ID"),
			inverseJoinColumns = @JoinColumn(name="INSTRUMENT_ID"))
	private Set<Instrument> instruments = new HashSet<Instrument>(); //Assuming an Exchange deals in various instruments then 1:* from Exchange to Instrument
	
	public Exchange() {}

	public Exchange(String exchangeId, String name) {
		this.exchangeId = exchangeId;
		this.name = name;
	}

	public Exchange(String exchangeId, String name, String markerHoursFrom, String marketHoursTo, TimeZone timeZone) {
		this.exchangeId = exchangeId;
		this.name = name;
		this.markerHoursFrom = markerHoursFrom;
		this.marketHoursTo = marketHoursTo;
		this.timeZone = timeZone;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public TimeZone getTimeZone() {
		return timeZone;
	}
	
	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}

	public String getExchangeId() {
		return this.exchangeId;
	}

	public void setExchangeId(String exchangeId) {
		this.exchangeId = exchangeId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getMarkerHoursFrom() {
		return this.markerHoursFrom;
	}

	public void setMarkerHoursFrom(String markerHoursFrom) {
		this.markerHoursFrom = markerHoursFrom;
	}

	public String getMarketHoursTo() {
		return this.marketHoursTo;
	}

	public void setMarketHoursTo(String marketHoursTo) {
		this.marketHoursTo = marketHoursTo;
	}
	
	public Set<Instrument> getInstruments() {
		return instruments;
	}

	public void setInstruments(Set<Instrument> instId) {
		this.instruments = instId;
	}
	
	public int getVersion() {
		return version;
	}
	
	public void setVersion(int version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return "Exchange [exchangeId=" + exchangeId + ", name=" + name + ", markerHoursFrom=" + markerHoursFrom
				+ ", marketHoursTo=" + marketHoursTo + ", timeZone=" + timeZone + ", version=" + version
				+ ", instruments=" + instruments + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((exchangeId == null) ? 0 : exchangeId.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Exchange other = (Exchange) obj;
		if (exchangeId == null) {
			if (other.exchangeId != null)
				return false;
		} else if (!exchangeId.equals(other.exchangeId))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	
	
	

	/*
	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}

	public int getMarketHoursDuration(int barSize) {
		if (barSize < 1) {
			throw new IllegalArgumentException("bar size too small ");
		}
		if (java.util.Objects.equals(markerHoursFrom, "9:30 a.m.")
				&& java.util.Objects.equals(marketHoursTo, "4:00 p.m.")) {
			return 390 / barSize;
		} else {
			throw new IllegalArgumentException("something wrong ");
		}
	}

	public int getMarketHoursDurationInMin() {
		if (java.util.Objects.equals(markerHoursFrom, "9:30 a.m.")
				&& java.util.Objects.equals(marketHoursTo, "4:00 p.m.")) {
			return 390;
		} else {
			throw new IllegalArgumentException("something wrong ");
		}
	}*/

	/*@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Exchange)) {
			return false;
		}
		Exchange exchange = (Exchange) o;
		return Objects.equal(exchangeId, exchange.exchangeId) && Objects.equal(name, exchange.name);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(exchangeId, name);
	}

	@Override
	public String toString() {
		return "Exchange{" + "exchangeId='" + exchangeId + '\'' + ", name='" + name + '\'' + "} " + super.toString();
	}*/
}
