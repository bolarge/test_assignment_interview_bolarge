package com.gjj.igden.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gjj.igden.referencetypes.State;


/*@NamedQueries({
	@NamedQuery(name="Account.findAll",
			    query="select a from Account a"), 
	@NamedQuery(name="Account.findById", 
			    query="select distinct a from Account a left join fetch a.watchLists w where a.id = :id"),
	})
@SqlResultSetMapping(
		name="accountResult",
		entities=@EntityResult(entityClass=Account.class))*/
@Entity
@Table(name = "ACCOUNT")
public class Account implements  Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// Properties
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;
	
	@Version
	@Column(name = "VERSION")
	private int version = 0;
	
	@NotEmpty
	@Column(name = "EMAIL", unique = true, nullable = false)
	private String email;
	
	@Column(name = "PASSWORD", nullable=true)
	private String password;
	
	@Basic(fetch = FetchType.LAZY)
	@Lob
	@Column(name = "IMAGE", nullable=true)
	private byte[] image;
	
	@Column(name="STATE", nullable=true)
	private String state = State.ACTIVE.getState();
	
	@Column(name = "ENABLED")
	private boolean enabled = true;
	
	@NotEmpty
	@Column(name = "ACCOUNT_NAME",nullable=false)
	private String accountName;
	
	@NotEmpty
	@Column(name = "ADDITIONAL_INFO", nullable=false)
	private String additionalInfo;
	
	@Column(name = "CREATION_DATE", nullable=true)
	@Temporal(TemporalType.DATE)
	private Date creationDate = new Date();
	
	//Relation
	@NotEmpty
	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "ACCOUNT_WATCHLIST_DETAIL", joinColumns = @JoinColumn(name = "ACCOUNT_ID"), 
	inverseJoinColumns = @JoinColumn(name = "WATCHLIST_ID"))
	private Set<WatchListDesc> watchLists = new HashSet<WatchListDesc>(); // *:* from WatchList, An Account can have
	
	@NotEmpty
	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "ACCOUNT_PROFILE", 
			joinColumns = @JoinColumn(name = "ACCOUNT_ID"),
			inverseJoinColumns = @JoinColumn(name="ROLE_ID"))
	private Set<UserProfile> userProfiles = new HashSet<UserProfile>();
	
	// Constructor
	public Account() {}
	
	public Account(String email, String password, String accountName, String additionalInfo, Date creationDate) {
		super();
		this.email = email;
		this.password = password;
		this.accountName = accountName;
		this.additionalInfo = additionalInfo;
		this.creationDate = creationDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
		
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
	
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	public byte[] getImage() {
		return this.image;
	}

	public void setImage(byte[] photo) {
		this.image = photo;
	}

	public boolean isEnabled() {
		return this.enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
		
	public String getState() {
        return state;
    }
 	
    public void setState(String state) {
        this.state = state;
    }

	public String getAdditionalInfo() {
		return this.additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}
	
	public String getAccountName() {
		return this.accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
		
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public Set<WatchListDesc> getWatchLists() {
		return this.watchLists;
	}

	public void setWatchLists(Set<WatchListDesc> watchLists) {
		this.watchLists = watchLists;
	}

	public Set<UserProfile> getUserProfiles() {
		return userProfiles;
	}

	public void setUserProfiles(Set<UserProfile> roles) {
		this.userProfiles = roles;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Account))
			return false;
		Account other = (Account) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Account [email=" + email + ", state=" + state + ", accountName=" + accountName + ", additionalInfo="
				+ additionalInfo + ", creationDate=" + creationDate  + "]";
	}
	
	
}
