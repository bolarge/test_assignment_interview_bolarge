package com.gjj.igden.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.security.core.userdetails.User;

@Embeddable
@Entity
@Table(name = "USER_ROLE")
@AssociationOverrides({
		@AssociationOverride(name = "pk.user", joinColumns = @JoinColumn(name = "USER_ID")),
		@AssociationOverride(name = "pk.role", joinColumns = @JoinColumn(name = "ROLE_ID"))})
public class UserRole implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private UserRolePK pk = new UserRolePK();
	private String createdBy;
	private Date created;

	public UserRole() {
	}

	@EmbeddedId
	public UserRolePK getPk() {
		return pk;
	}

	public void setPk(UserRolePK pk) {
		this.pk = pk;
	}
	
	@Transient
	public Users getUser() {
		return getPk().getUser();
	}

	public void setUser(Users user) {
		getPk().setUser(user);
	}

	@Transient
	public Role getRole() {
		return getPk().getRole();
	}

	public void setRole(Role role) {
		getPk().setRole(role);
	}
	
	@Column(name = "CREATED_BY", length=15)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pk == null) ? 0 : pk.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserRole other = (UserRole) obj;
		if (pk == null) {
			if (other.pk != null)
				return false;
		} else if (!pk.equals(other.pk))
			return false;
		return true;
	}
	
}
