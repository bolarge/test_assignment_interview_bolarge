package com.gjj.igden.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gjj.igden.referencetypes.State;

@Table(name="APP_USERS")
@Entity
public class Users implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "VERSION")
	private int version = 0;
	
	@Column(name = "NAME")
	private String name;
		
	/*@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@DateTimeFormat(iso = ISO.DATE)
	@Temporal(TemporalType.DATE)*/
	@Column(name = "DOB")
	private String dob;
	
	@Column(name = "EMAIL", unique = true, nullable = false)
	private String email;
	
	@Column(name = "PHONE")
	private String phone;
	
	@Column(name = "PASSWORD")
	private String password;
	
	@Column(name="STATE", nullable=true)
	private String state = State.ACTIVE.getState();

	//Relations
	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "USERS_USER_PROFILE", 
			joinColumns = @JoinColumn(name = "USER_ID"),
			inverseJoinColumns = @JoinColumn(name="ROLE_ID"))
	private Set<UserProfile> userProfiles = new HashSet<UserProfile>();
	//private Set<OperationParameters> operationParameters = new HashSet<OperationParameters>(); //LazyList.lazyList(new ArrayList<>(),FactoryUtils.instantiateFactory(OperationParameters.class));
	
	//Constructor
	public Users() {}
	
	public Users(String name, String dob, String email, String phone) {
		this.name = name;
		this.dob = dob;
		this.email = email;
		this.phone = phone;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
		
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
		
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	   
	public String getName() {
		return name;
	}
    
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}
	
	/*@Transient
	public String getBirthDateString() {
		String dobString = "";
		if (this.dob != null)
			dobString = org.joda.time.format.DateTimeFormat.forPattern("dd-MM-yyyy").print(this.dob);
		return dobString;
	}*/
  
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public Set<UserProfile> getUserProfiles() {
		return userProfiles;
	}

	public void setUserProfiles(Set<UserProfile> roles) {
		this.userProfiles = roles;
	}
	
	public String getState() {
        return state;
    }
 	
    public void setState(String state) {
        this.state = state;
    }
	
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	/*@ElementCollection
	@CollectionTable(
			name="OPERATIONPARAMETERS",
			joinColumns=@JoinColumn(name="USER_ID"))
	@AttributeOverride(name="name",
	column=@Column(name="USER_OPERATION_PARAMS",unique=true,nullable=false))
	public Set<OperationParameters> getOperationParameters() {
		return operationParameters;
	}

	public void setOperationParameters(Set<OperationParameters> operationParameters) {
		this.operationParameters = operationParameters;
	}*/

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Users))
			return false;
		Users other = (Users) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Users [version=" + version + ", name=" + name + ", dob=" + dob + ", email=" + email + ", phone=" + phone
				+ "]";
	}
}