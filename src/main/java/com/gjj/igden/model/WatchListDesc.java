package com.gjj.igden.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Table(name="DATA_SET")
@Entity
@NamedQueries({
	@NamedQuery(name="WatchListDesc.findAll",
			    query="select w from WatchListDesc w"), 
	@NamedQuery(name="WatchListDesc.findByWatchListId", 
			    query="select distinct w from WatchListDesc w left join fetch w.accounts a where w.watchListId = :watchListId"),
	})
@SqlResultSetMapping(
		name="watchListDescResult",
		entities=@EntityResult(entityClass=WatchListDesc.class))
public class WatchListDesc implements Serializable{ // implements IWatchListDesc

	// Properties					//lack of clarity in how the objects have been named, difficult to sieve out what could have been meaningful ordinary
	private Integer watchListId; 	//This class should qualify as an entity for it describes a thing or a service. by that margin it should be self identifiable
	private String watchListName;
	private String watchListDetails;
	private String dataProviders;
	private int marketDataFrequency; //
	// Relation
	private Set<Account> accounts = new HashSet<Account>();  //Change type Account //*:* from Account,  A WatchList can have more than one related Account	
	private List<String> stockSymbolsList = new ArrayList<String>(); //Collection property, will be better to create a stock symbols object rather than treating as String 
	private List<OperationParameters> operationParameters = new ArrayList<OperationParameters>();//LazyList.lazyList(new ArrayList<>(), FactoryUtils.instantiateFactory(OperationParameters.class));  //Added type, property is *:1 from OperationParameter

	// Constructor
	public WatchListDesc() {
	}

	public WatchListDesc(Integer watchListId, Set<Account> accounts, String watchListName, String watchListDetails,
			int marketDataFrequency, String dataProviders) {
		this.watchListId = watchListId;
		this.accounts = accounts;
		this.watchListName = watchListName;
		this.watchListDetails = watchListDetails;
		this.marketDataFrequency = marketDataFrequency;
		this.dataProviders = dataProviders;
	}

	public WatchListDesc(Set<Account> accounts) { //int accountId
		this.accounts = accounts;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "DATA_SET_ID")
	public Integer getWatchListId() {
		return this.watchListId;
	}

	public void setWatchListId(Integer watchListId) {
		this.watchListId = watchListId;
	}

	@Column(name = "DATA_SET_NAME")
	public String getWatchListName() {
		return this.watchListName;
	}

	public void setWatchListName(String watchListName) {
		this.watchListName = watchListName;
	}

	@Column(name = "DATA_SET_DESCRIPTION")
	public String getWatchListDetails() {
		return this.watchListDetails;
	}

	public void setWatchListDetails(String watchListDetails) {
		this.watchListDetails = watchListDetails;
	}

	@Column(name = "DATA_PROVIDERS")
	public String getDataProviders() {
	
		return this.dataProviders;
	}

	public void setDataProviders(String dataProviders) {
		this.dataProviders = dataProviders;
	}

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Account.class)
	@JoinColumn(name="ACCOUNT_FK_ID")
	public Set<Account> getAccounts() { //formerly of type int 
		return this.accounts;
	}

	public void setAccounts(Set<Account> accounts) { //formerly of type int
		this.accounts = accounts;
	}

	@ElementCollection(targetClass=OperationParameters.class)
	public List<OperationParameters> getOperationParameterses() {
		return this.operationParameters;
	}

	public void setOperationParameterses(List<OperationParameters> operationParameters) {
		this.operationParameters = operationParameters;
	}

	@Column(name = "MARKET_DATA_FREQUENCY")
	public int getMarketDataFrequency() {
		return this.marketDataFrequency;
	}

	public void setMarketDataFrequency(int marketDataFrequency) {
		this.marketDataFrequency = marketDataFrequency;
	}

	@ElementCollection
	public List<String> getStockSymbolsList() {
		return this.stockSymbolsList;
	}

	public void setStockSymbolsList(List<String> stockSymbolsList) {
		this.stockSymbolsList = stockSymbolsList;
	}

	/*public void setStockSymbolsListFromOperationList(List<OperationParameters> stockSymbolsList) {
		List<String> stringList = stockSymbolsList.stream().map(OperationParameters::getName)
				.collect(Collectors.toList());
		this.stockSymbolsList = stringList;
	}*/

	/*@Override
	public String toString() {
		return String.valueOf(" account id =  " + this.getAccountId() + "\n " + "data set id = " + this.getWatchListId()
				+ "\n " + "market data freq = " + this.getMarketDataFrequency() + "\n " + "data set name = "
				+ this.getWatchListName() + "\n " + "data set description = " + this.getWatchListDetails() + "\n ");
	}*/
}