	package com.gjj.igden.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@SuppressWarnings("serial")
@MappedSuperclass
public abstract class MarketData implements Serializable{ // public abstract class MarketData
	
	@Id @Column(name="ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	protected Long id;
	@Column(name = "MD_ID")
	protected Integer marketDataId;
	@Column(name="DATE")
	protected Date dateTime;
	@Column(name="TICKER")
	protected String ticker;
	@Column(name="VERSION")
	private int version;
	//Relations
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Instrument.class)
	@JoinColumn(name="INSTID_FK")
	protected Instrument instrument; //*:1 

	protected MarketData() {}

	protected MarketData(Instrument instId, Date dateTime) {
		this.instrument = instId;
		this.dateTime = dateTime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	public Integer getMarketDataId() {
		return marketDataId;
	}

	public void setMarketDataId(Integer marketDataId) {
		this.marketDataId = marketDataId;
	}

	
	public Instrument getInstId() {
		return this.instrument;
	}

	public void setInstId(Instrument instId) {
		this.instrument = instId;
	}

	
	public String getTicker() {
		return this.ticker;//instrument.getSymbol();
	}
	
	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

	
	public Date getDateTime() {
		return dateTime;
	}
	
	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}
	
	
	public int getVersion() {
		return version;
	}
	
	public void setVersion(int version) {
		this.version = version;
	}

	public Instrument getInstrument() {
		return instrument;
	}

	public void setInstrument(Instrument instrument) {
		this.instrument = instrument;
	}

	@Override
	public String toString() {
		return "MarketData [dateTime=" + dateTime + ", marketDataId=" + marketDataId + ", ticket=" + ticker
				+ ", version=" + version + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((marketDataId == null) ? 0 : marketDataId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof MarketData))
			return false;
		MarketData other = (MarketData) obj;
		if (marketDataId == null) {
			if (other.marketDataId != null)
				return false;
		} else if (!marketDataId.equals(other.marketDataId))
			return false;
		return true;
	}

}
