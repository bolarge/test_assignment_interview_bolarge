package com.gjj.igden.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Service;

@Service
public class CustomSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

	final Logger logger = LoggerFactory.getLogger(CustomSuccessHandler.class);
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

	@Override
	protected void handle(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException {
		String targetUrl = determineTargetUrl(authentication);

		if (response.isCommitted()) {
			System.out.println("Can't redirect");
			logger.info("Can't redirect");
			return;
		}

		redirectStrategy.sendRedirect(request, response, targetUrl);
	}

	/* * This method extracts the roles of currently logged-in user and returns
	 * appropriate URL according to his/her role.*/
	 
	protected String determineTargetUrl(Authentication authentication) {
		String url = "";
		
		/*String[] targetUrl = {"/acc_mngr","/mkd_mngr","/wsl_mngr","/usr_mngr","/ins_mngr","/xch_mngr"};
		List<String> abc = new ArrayList<String>();
		abc.add("/acc_mngr");abc.add("/mkd_mngr");abc.add("/wsl_mngr");abc.add("/usr_mngr");abc.add("/ins_mngr");abc.add("/xch_mngr");
*/
		Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();

		List<String> roles = new ArrayList<String>();

		for (GrantedAuthority a : authorities) {
			roles.add(a.getAuthority());
		}

		if (isDba(roles)) {
			logger.info("Test is DBA");
			url = "/dba";
		} else if (isAdmin(roles)) {  //Implement Switch to evaluate for multiple outcomes
			logger.info("Test is ADMIN");
			url = "/mkd_mngr";	//url = "/wsl_mngr";	
		} else if (isUser(roles)) {
			logger.info("Test is USER");
			url = "/home";
		} else {
			logger.info("Test if DENIED");
			url = "/access_denied";
		}
		return url;
	}

	private boolean isUser(List<String> roles) {
		if (roles.contains("ROLE_USER")) {
			return true;
		}
		return false;
	}

	private boolean isAdmin(List<String> roles) {
		if (roles.contains("ROLE_ADMIN")) {
			return true;
		}
		return false;
	}

	private boolean isDba(List<String> roles) {
		if (roles.contains("ROLE_DBA")) {
			return true;
		}
		return false;
	}

	public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
		this.redirectStrategy = redirectStrategy;
	}

	protected RedirectStrategy getRedirectStrategy() {
		return redirectStrategy;
	}

}