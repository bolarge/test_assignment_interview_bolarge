package com.gjj.igden.utils;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.Part;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class UploadableServiceImpl implements UploadableService{
	
	private final Logger logger = LoggerFactory.getLogger(UploadableServiceImpl.class);

	@Override
	public byte[] uploadContent(Part file) {
		byte[] fileContent = null;
		//Process content
		if (file != null) {
		      this.logger.info("File name: " + file.getName());
		      this.logger.info("File size: " + file.getSize());
		      this.logger.info("File content type: " + file.getContentType());
		      fileContent = (byte[])null;
		      try {
		        InputStream inputStream = file.getInputStream();
		        if (inputStream == null)
		          this.logger.info("File inputstream is null");
		        fileContent = IOUtils.toByteArray(inputStream);
		        //account.setImage(fileContent);
		      } catch (IOException localIOException) {
		        this.logger.error("Error saving uploaded file");
		      }
		      //return fileContent;
		    }
		return fileContent;
	}
}
