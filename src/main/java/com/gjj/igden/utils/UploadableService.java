package com.gjj.igden.utils;

import javax.servlet.http.Part;

public interface UploadableService {
	
	byte[] uploadContent(Part file);

}
