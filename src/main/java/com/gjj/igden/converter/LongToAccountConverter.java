package com.gjj.igden.converter;

import org.springframework.core.convert.converter.Converter;

import com.gjj.igden.model.Account;
import com.gjj.igden.service.AccountService;

public class LongToAccountConverter implements Converter<Object, Object> {

	public LongToAccountConverter() {
		super();
	}

	private AccountService accountService;

	public LongToAccountConverter(AccountService accountService) {
		this.accountService = accountService;
	}	

	public Object convertTargetToSourceClass(Account target, Class<Long> sourceClass)
			throws Exception {
		Account anAccount = target;
		return anAccount.toString();
	}

	@Override
	public Object convert(Object source){
		Integer longSource = (Integer) source;
		if (longSource != null && longSource > 0) {
			return accountService.findById(longSource);
		} else {
			return null;
		}
	}
}
