package com.gjj.igden.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import com.gjj.igden.model.UserProfile;
import com.gjj.igden.service.UserProfileService;

public class UserProfileConverter implements Converter<String, UserProfile>{
	
	private UserProfileService userProfileService;

	public UserProfileConverter() {
		super();
	}

	@Autowired
	public UserProfileConverter(UserProfileService userProfileService) {
		this.userProfileService = userProfileService;
	}
	
	@Override
	public UserProfile convert(String target) {
		String stringSource = target;
		if (stringSource != null && stringSource.length() > 0) {
			return userProfileService.findById(Integer.valueOf(stringSource));
		} else {
			return null;
		}
	}
	
}