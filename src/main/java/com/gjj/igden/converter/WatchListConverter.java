package com.gjj.igden.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import com.gjj.igden.model.WatchListDesc;
import com.gjj.igden.service.WatchListDescService;

public class WatchListConverter implements Converter<String, WatchListDesc>{
	
	private WatchListDescService watchListService;
	
	@Autowired
	public WatchListConverter(WatchListDescService watchListService) {
		this.watchListService= watchListService;
	}

	public WatchListConverter() {
		super();
	}

	@Override
	public WatchListDesc convert(String source) {
		String stringSource = source;
		if (stringSource != null && stringSource.length() > 0) {
			return watchListService.findById(Integer.valueOf(stringSource));
		} else {
			return null;
		}
	}
}
