package com.gjj.igden.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.userdetails.User;

import com.gjj.igden.model.Account;
import com.gjj.igden.service.AccountService;

public class StringToAccountConverter implements Converter<String, Account> {

	@Autowired
	private AccountService accountService;

	/*public StringToAccountConverter(AccountService accountService) {
		this.accountService = accountService;
	}*/

	@Override
	public Account convert(String target) {
		String stringSource = target;
		if (stringSource != null && stringSource.length() > 0) {
			return accountService.findById(Integer.valueOf(stringSource));
		} else {
			return null;
		}
	}

	public StringToAccountConverter() {
		super();
	}

	public Object convertSourceToTargetClass(Object source, Class<User> targetClass) throws Exception {
		if (source != null)
			return ((User) source).toString();
		else
			return null;
	}

	public Class<String> getSourceClass() {
		return String.class;
	}

	public Class<Account> getTargetClass() {
		return Account.class;
	}
}
