package com.gjj.igden.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import com.gjj.igden.model.Bar;
import com.gjj.igden.service.BarService;

public class BarConverter implements Converter<String, Bar>{

	private  BarService barService;
	
	@Autowired
	public void setBarService(BarService barService) {
		this.barService = barService;
	}

	public BarConverter() {
		super();
	}

	@Override
	public Bar convert(String source) {
		String stringSource = source;
		if (stringSource != null && stringSource.length() > 0) {
			return barService.findById(Long.valueOf(stringSource));
		} else {
			return null;
		}
	}

}
