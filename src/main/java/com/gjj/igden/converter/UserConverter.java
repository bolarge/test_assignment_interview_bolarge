package com.gjj.igden.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import com.gjj.igden.model.Users;
import com.gjj.igden.service.UsersService;

public class UserConverter implements Converter<String, Users>{
	
	private UsersService userService;
	
	@Autowired
	public UserConverter(UsersService userService) {
		this.userService= userService;
	}

	public UserConverter() {
		super();
	}

	@Override
	public Users convert(String source) {
		String stringSource = source;
		if (stringSource != null && stringSource.length() > 0) {
			return userService.findById(Long.valueOf(stringSource));
		} else {
			return null;
		}
	}
}
