package com.gjj.igden.converter;

import org.springframework.core.convert.converter.Converter;

import com.gjj.igden.service.UserProfileService;

public class IntegerToUserProfileConverter implements Converter<Object, Object>{
	
	private UserProfileService userProfileService;
	
	public IntegerToUserProfileConverter(UserProfileService userProfileService) {
		this.userProfileService = userProfileService;
	}

	@Override
	public Object convert(Object source){
		Integer longSource = (Integer) source;
		if (longSource != null && longSource > 0) {
			return userProfileService.findById(longSource);
		} else {
			return null;
		}
	}

	public IntegerToUserProfileConverter() {
		super();
	}
	
	

}
