<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<div id="mainSection" xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:spring="http://www.springframework.org/tags"
	xmlns:sec="http://www.springframework.org/security/tags" version="2.0">

	<jsp:directive.page contentType="text/html;charset=UTF-8" />
	<jsp:output omit-xml-declaration="yes" />

	<div>
		Dear <strong>${user}</strong>, Welcome to DBA Page. <a
			href="<c:url value="/logout" />">Logout</a>
	</div>
</div>