package com.gjj.igden.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(MarketData.class)
public abstract class MarketData_ {

	public static volatile SingularAttribute<MarketData, Date> dateTime;
	public static volatile SingularAttribute<MarketData, String> ticker;
	public static volatile SingularAttribute<MarketData, Instrument> instrument;
	public static volatile SingularAttribute<MarketData, Long> id;
	public static volatile SingularAttribute<MarketData, Integer> marketDataId;
	public static volatile SingularAttribute<MarketData, Integer> version;

}

