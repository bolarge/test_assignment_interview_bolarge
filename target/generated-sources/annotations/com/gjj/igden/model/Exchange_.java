package com.gjj.igden.model;

import java.util.TimeZone;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Exchange.class)
public abstract class Exchange_ {

	public static volatile SingularAttribute<Exchange, String> exchangeId;
	public static volatile SingularAttribute<Exchange, String> marketHoursTo;
	public static volatile SetAttribute<Exchange, Instrument> instruments;
	public static volatile SingularAttribute<Exchange, String> name;
	public static volatile SingularAttribute<Exchange, String> markerHoursFrom;
	public static volatile SingularAttribute<Exchange, TimeZone> timeZone;
	public static volatile SingularAttribute<Exchange, Integer> id;
	public static volatile SingularAttribute<Exchange, Integer> version;

}

