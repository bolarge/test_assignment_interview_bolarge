package com.gjj.igden.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(WatchListDesc.class)
public abstract class WatchListDesc_ {

	public static volatile ListAttribute<WatchListDesc, String> stockSymbolsList;
	public static volatile SingularAttribute<WatchListDesc, String> watchListName;
	public static volatile SingularAttribute<WatchListDesc, Integer> marketDataFrequency;
	public static volatile SingularAttribute<WatchListDesc, String> watchListDetails;
	public static volatile SingularAttribute<WatchListDesc, Integer> watchListId;
	public static volatile SingularAttribute<WatchListDesc, String> dataProviders;
	public static volatile ListAttribute<WatchListDesc, OperationParameters> operationParameterses;
	public static volatile SetAttribute<WatchListDesc, Account> accounts;

}

