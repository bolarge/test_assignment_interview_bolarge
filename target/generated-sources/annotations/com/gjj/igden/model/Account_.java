package com.gjj.igden.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Account.class)
public abstract class Account_ {

	public static volatile SingularAttribute<Account, byte[]> image;
	public static volatile SingularAttribute<Account, String> password;
	public static volatile SingularAttribute<Account, String> accountName;
	public static volatile SingularAttribute<Account, String> additionalInfo;
	public static volatile SetAttribute<Account, UserProfile> userProfiles;
	public static volatile SingularAttribute<Account, Long> id;
	public static volatile SingularAttribute<Account, String> state;
	public static volatile SingularAttribute<Account, Date> creationDate;
	public static volatile SingularAttribute<Account, Integer> version;
	public static volatile SetAttribute<Account, WatchListDesc> watchLists;
	public static volatile SingularAttribute<Account, String> email;
	public static volatile SingularAttribute<Account, Boolean> enabled;

}

