package com.gjj.igden.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Greeting.class)
public abstract class Greeting_ {

	public static volatile SingularAttribute<Greeting, Long> id;
	public static volatile SingularAttribute<Greeting, String> content;

}

