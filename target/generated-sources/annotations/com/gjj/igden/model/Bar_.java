package com.gjj.igden.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Bar.class)
public abstract class Bar_ extends com.gjj.igden.model.MarketData_ {

	public static volatile SingularAttribute<Bar, Long> volume;
	public static volatile SingularAttribute<Bar, Integer> barSize;
	public static volatile SingularAttribute<Bar, Double> high;
	public static volatile SingularAttribute<Bar, Double> low;
	public static volatile SingularAttribute<Bar, Double> close;
	public static volatile SingularAttribute<Bar, Double> open;

}

