package com.gjj.igden.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Instrument.class)
public abstract class Instrument_ {

	public static volatile SingularAttribute<Instrument, String> symbol;
	public static volatile SingularAttribute<Instrument, Exchange> exchange;
	public static volatile SingularAttribute<Instrument, Integer> version;

}

