package com.gjj.igden.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Users.class)
public abstract class Users_ {

	public static volatile SingularAttribute<Users, String> password;
	public static volatile SingularAttribute<Users, String> phone;
	public static volatile SingularAttribute<Users, String> dob;
	public static volatile SingularAttribute<Users, String> name;
	public static volatile SetAttribute<Users, UserProfile> userProfiles;
	public static volatile SingularAttribute<Users, Long> id;
	public static volatile SingularAttribute<Users, Integer> version;
	public static volatile SingularAttribute<Users, String> email;

}

