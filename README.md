# README #

##Project Update
Presentation layer completed. Views upgraded to JSP 2 but maintained use of bootstrap, jstl front end technologies. Security  at web tier and Persistent login implemented. Began looking into Social application.   


Completed re-work of service and data access layer, now it is Hibernate + JPA(JPQL, NamedQueries and Criteria). Application is nearly ready as all parts are now in sync. However, i still have lots of views that I am yet to review and Social network project to complete integration. I will need to revise the application but the time i have left is no longer sufficient as a result may I request more time to enable me finish up.

Delivery is 5.30PM today, 25/10/2017. If my request is granted i will need to update my time-line.
 
Completed re-structuring of presentation layer views base on a logical order and object representation
Completed review of Spring JPA 1 But yet to complete review for Spring JPA2 and I still have Social Project.
Basic CRUD operations across all Object from database up to their controllers levels is completed (Spring Data, JPA + Hibernate in Action). What I have left to finish up is exposing application operations into their respective views templates. I will be requesting more time at 8 hours more at this point as the remaining time is now insufficient to cover what is left.  

I have 7.5 hours left and now requesting addition 8 hours. My delivery date is 24/10/2017, If request is granted delivery date will become 25/10/2017

Completed remaining services interfaces/services implementation from base project 
Completed Security and tested login feature. Began Presentation layer, Views will be structured into folders based on the object represented. On completion of work on presentations, I will then the roll back base project into its multi module architecture as originally designed.  

I will be reviewing further Spring JPA project 1 & 2 later this afternoon to ensure I have kept in line with the requirements and as a result make necessary adjustments.

I will be completing the project within the next 13.5 working hours. Delivery is Tuesday, 24/10/2017.  


This README would normally document whatever steps are necessary to get your application up and running.

##Business Process Not Given


Test Assignment task is a migration project of Spring MVC with JDBCTemplate data access to Spring MVC Sprint Data and JPA

Time estimation for the projects should take about 24 hours *rough guess* 

My approach is to structure my review into BackEnd and FrontEnd activities. In that logical order there will be just two
review structure. 

Then I will begin from the Backend Structure breaking it into its individual sub activities. The backend 
structure will comprise the folowing; domain layer, service  layer and controller layer. Each of these sub activities is then
broken into various tasks.

Same devide and conquer approach will be applied to the FrontEnd Activitiy, Sub activities her will include view templates,
layout of the view template, whether the views flow in a pattern(MVC) or not

24 working hours estimate will then be a 3 Days timeline. Break down are as follows
BackEnd Activities (Domain, Service, Controlller Layers) 2 Days Due on 21/10/2017
FrontEnd Activities (Controller Layer, Presentaion Views) 1 Days Due on 22/10/2017

##Timeline Update 
As a result of Initial Study and Analysis of Code Base and setups, I will adjust my time spending to exclude the phase. This brings my start time to today

BackEnd Activities (Domain, Service, Controlller Layers) 2 Days Due on 22/10/2017
FrontEnd Activities (Controller Layer, Presentaion Views) 1 Days Due on 23/10/2017

Starting from 8AM, This is my third commit of the day. 6 Hours of working gone so far
Time left is 2 1/4 days.

Please note Time is counted in business working hours. 8 hours per day

##Timeline Update 22/10/2017
Start Time today was 2.00PM (Got back from Sunday Worship past 1PM, had some meal and some relaxation before resuming) 
First Commit Time Spent 2.5hours
Time Balance 17.5 Hours/ 1 1/8 days.

Project Due time is adjusted with time balance to bring the correct due to  early hours of 24/10/2017


##Adapt Spring-mvc-jdbc to single module from multi module
Observation from the code review: What was the reason for making this application a multi module?
From the size the model objects (8 in all), 8 interacting classes will be fine in a single module. Less time is required to maintain than in multi module

Code now exist in the upgrade application using three layer abstraction integration/service and presentation layers


##Update/Refactor domain objects from plain classes to persistent JPA object

Account Class Update
--------------------
Annotate Account class @Entity and decorated its properties at accessor level with @Column mapping
property eMail of Account class has two accessor methods, disabled one and used the other sticking to the java bean rule

WatchListDesc Class Update
--------------------------
WatchListDesc implements IWatchListDesc interface making it a polymorphic class enjoying multi behavior *thumbs up here*. In the spirit separation of concern i will
refine WatchListDesc to serve as a pojo that it should be, then move IWatchListDesc to the service layer. This frees it from unnecessary responsibilities.

MarketData Class Updater
------------------------
MarketData Class is abstract and none of the other domain classes extends it *this is itchy*. Then what is it doing here?

OperationParameters Class Refactor
------------------------------------
According to the Fintech_ws Schema, there is no table for OperationParameter class.OperationParameters class in my view is a reference object requiring no form of self identity. In that wise,  OperationParameter class does not qualify to me regarded as class(Entity) but a (Value) property of another class.

Other consideration is that if OperationParameters is used to retrieve a large list of object, still does not make it qualify to be an Entity by right rather a collection of property values. By this Operation can be defined as an @Embeddable and then mapped an @ElementCollection

Am going with my other consideration, define OperationParameters as @Embeddable

Other classes not making sense now are Exchange, InstId and these two should relate to WatchListDesc

##Data Access/Repository Definition
AccountReposity, BarRepository, MarketDatRepository and WatchListDescRepository all defined

##Services Interface and Implementation
AccountService, BarService, WatchListDesc service all defined 

##SpringJDBCTemplate Re-Design Status
Completed data model redesign.
Completed domain model design
Completed JPA repository
Completed Services Interface
Completed Quarter of Total Services Interface Implementation Classes




 
 

